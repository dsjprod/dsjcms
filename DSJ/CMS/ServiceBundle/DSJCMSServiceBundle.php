<?php

namespace DSJ\CMS\ServiceBundle;

use DSJ\CMS\ServiceBundle\DependencyInjection\DSJCMSExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DSJCMSServiceBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new DSJCMSExtension();
    }
}
