<?php
namespace DSJ\CMS\ServiceBundle\Manager\Backoffice;

use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Router;

class LayoutManager extends ContainerAware
{
    /**
     * @var Router $oRouting
     */
    private $oRouting;

    public function __construct($oRouting) {
        $this->oRouting = $oRouting;
    }
    public function getFetchLayout($aEntities, $aColumns, $sEditRoute=null, $sDeleteRoute=null, $sCustomColumn=null)
    {
        $aReturn=array();
        foreach($aEntities as $iKey => $oEntity) {
            foreach($aColumns as $aField) {
                if(array_key_exists('fetch', $aField) === false) { continue; }
                $sFieldValue = '';

                // create URL if needed
                if(array_key_exists('link', $aField)) {
                    // create array with parameters for URL
                    $aUrlVars = array();
                    if(array_key_exists('vars', $aField['link'])) {
                        foreach($aField['link']['vars'] as $sField => $sValue) {
                            if(stristr($sValue, '.') !== false) {
                                $aRelation = explode('.', $sValue);
                                $i = 0;
                                $var=$oEntity;
                                while($i < count($aRelation)) {
                                    if($var === NULL || $var === false) {
                                        $var = null;
                                        break;
                                    }

                                    if (stristr($aRelation[$i], '(') !== false) {
                                        $aRelation = explode('(', $aRelation[$i]);
                                        $var = call_user_func(array($var, $aRelation[0]), str_replace(')', '', $aRelation[1]));
                                    } else {
                                        $var = call_user_func(array($var, $aRelation[$i]));
                                    }
                                    $i++;
                                }
                            } else {
                                $var = call_user_func(array($oEntity, $sValue));
                            }
                            if($var === NULL) {
                                $aReturn['data'][$iKey][] = '';
                                continue 2;
                            }
                            $aUrlVars[$sField] = $var;
                        }
                    }

                    // create string of attributes for a element
                    $sAttr = '';
                    if(array_key_exists('attr', $aField['link'])) {
                        foreach($aField['link']['attr'] as $sKey => $sAttrValue) {
                            $sAttr .= $sKey.'="'.$sAttrValue.'" ';
                        }
                    }

                    // generate URL
                    $sFieldValue .= '<a href="'.$this->oRouting->generate($aField['link']['path'], $aUrlVars).'" '.$sAttr.'>' . (array_key_exists('text', $aField['link']) ? $aField['link']['text'] : '');
                }

                // create input field if needed
                if(array_key_exists('input', $aField)) {
                    if (array_key_exists('type', $aField['input'])) {
                        if ($aField['input']['type'] == 'select') {
                            if(is_callable($aField['input']['choices']) || is_callable(stripslashes($aField['input']['choices']))) {
                                if(is_callable($aField['input']['choices']))
                                    $aChoices = call_user_func($aField['input']['choices']);
                                elseif(is_callable(stripslashes($aField['input']['choices'])))
                                    $aChoices = call_user_func(stripslashes($aField['input']['choices']));
                            } elseif(method_exists($oEntity, $aField['input']['choices'])) {

                                if(array_key_exists('parameter', $aField['input'])) {
                                    $parameter = json_decode($aField['input']['parameter']);
                                    if(empty($parameter)) {
                                        $parameter = json_decode(stripslashes($aField['input']['parameter']));
                                    }
                                    $aChoices = call_user_func(
                                        array($oEntity, $aField['input']['choices']),
                                        $parameter
                                    );
                                } else
                                    $aChoices = call_user_func(array($oEntity, $aField['input']['choices']));
                            } else {
                                $aChoices = json_decode($aField['input']['choices']);
                            }

                            $sAttr = '';
                            if (array_key_exists('attr', $aField['input'])) {
                                foreach ($aField['input']['attr'] as $sKey => $sAttrValue) {
                                    if (stristr($sAttrValue, '.') !== false) {
                                        $aRelation = explode('.', $sAttrValue);
                                        $i = 0;
                                        $var = $oEntity;
                                        while ($i < count($aRelation)) {
                                            if ($var === null || $var === false) {
                                                $var = null;
                                                break;
                                            }

                                            if (stristr($aRelation[$i], '(') !== false) {
                                                $aRelation = explode('(', $aRelation[$i]);
                                                $var = call_user_func(
                                                    array($var, $aRelation[0]),
                                                    str_replace(')', '', $aRelation[1])
                                                );
                                            } else {
                                                $var = call_user_func(array($var, $aRelation[$i]));
                                            }
                                            $i++;
                                        }
                                        $sAttrValue = $var;
                                    } elseif (method_exists($oEntity, $sAttrValue)) {
                                        $sAttrValue = call_user_func(array($oEntity, $sAttrValue));
                                    }
                                    $sAttr .= $sKey . '="' . $sAttrValue . '" ';
                                }
                            }
                            $iCurrentValue = null;
                            if (array_key_exists('currentValue', $aField['input'])) {
                                if (stristr($aField['input']['currentValue'], '.') !== false) {
                                    $aRelation = explode('.', $aField['input']['currentValue']);
                                    $i = 0;
                                    $var = $oEntity;
                                    while ($i < count($aRelation)) {
                                        if ($var === null || $var === false) {
                                            $var = null;
                                            break;
                                        }

                                        if (stristr($aRelation[$i], '(') !== false) {
                                            $aRelation = explode('(', $aRelation[$i]);
                                            $var = call_user_func(
                                                array($var, $aRelation[0]),
                                                str_replace(')', '', $aRelation[1])
                                            );
                                        } else {
                                            $var = call_user_func(array($var, $aRelation[$i]));
                                        }
                                        $i++;
                                    }
                                } else {
                                    $var = call_user_func(array($oEntity, $aField['input']['currentValue']));
                                }
                                $iCurrentValue = $var;
                            }

                            $sFieldValue .= '<select name="' . $aField['input']['name'] . '" ' . $sAttr . '>';
                            if (array_key_exists('emptyValue', $aField['input']) && $aField['input']['emptyValue'] == true) {
                                $sFieldValue .= '<option value=""></option>';
                            }
                            if(sizeof($aChoices) > 0) {
                                foreach ($aChoices as $iKeyChoice => $sChoice) {
                                    $sFieldValue .= '<option value="' . $iKeyChoice . '" ' . (intval(
                                            $iCurrentValue
                                        ) === intval($iKeyChoice) ? 'selected' : '') . '>' . $sChoice . '</option>';
                                }
                            }
                            $sFieldValue .= '</select>&nbsp;<span class="statusHolder"></span>';
                        }
                    }
                } else {
                    // create content
                    if ($aField['fetch'] !== true && $aField['fetch'] !== 'true') {
                        if (stristr($aField['fetch'], '.') !== false) {
                            $aRelation = explode('.', $aField['fetch']);
                            $i = 0;
                            $var = $oEntity;
                            while ($i < count($aRelation)) {
                                if ($var === null || $var === false) {
                                    if ($var !== false) {
                                        $var = '-';
                                    }
                                    break;
                                }

                                if (stristr($aRelation[$i], '(') !== false) {
                                    $aRelation = explode('(', $aRelation[$i]);
                                    $var = call_user_func(
                                        array($var, $aRelation[0]),
                                        str_replace(')', '', $aRelation[1])
                                    );
                                } else {
                                    $var = call_user_func(array($var, $aRelation[$i]));
                                }
                                $i++;
                            }
                            $sReturn = $var;
                        } else {
                            $sReturn = call_user_func(array($oEntity, $aField['fetch']));
                        }

                        if (is_bool($sReturn)) {
                            if ($sReturn === true) {
                                $sReturn = '<span title="1"></span><i class="fa fa-check text-success"></i>';
                            } else {
                                $sReturn = '<span title="0"></span><i class="fa fa-times text-danger"></i>';
                            }
                        }

                        if(array_key_exists('type', $aField) && $aField['type'] == 'numeric-comma') {
                            $sReturn = number_format($sReturn, 2, ',', '.');
                        }

                        if($sReturn instanceof \DateTime)
                        {
                            $sFieldValue .= $sReturn->format('d-m-Y H:i');
                        }
                        else
                        {
                            $sFieldValue .= $sReturn;
                        }
                    }

                    if (array_key_exists('link', $aField)) {
                        $sFieldValue .= '</a>';
                    }
                }

                $aReturn['data'][$iKey][] = ($sFieldValue == NULL ? '' : $sFieldValue);
            }

            if($sEditRoute !== NULL) {
                $aReturn['data'][$iKey][] = '<a href="' . $this->oRouting->generate($sEditRoute, array('id' => $oEntity->getId())) . '" class="btn btn-sm blue" title="Bewerken"><i class="fa fa-pencil"></i></a>';
            }
            if($sDeleteRoute !== NULL) {
                $aReturn['data'][$iKey][] = '<a href="' . $this->oRouting->generate($sDeleteRoute, array('id' => $oEntity->getId())) . '" class="btn btn-sm red" title="Verwijderen" onclick="return confirm(\'Weet u zeker dat u dit object wilt verwijderen?\');"><i class="fa fa-trash-o"></i></a>';
            }
            if($sCustomColumn == 'selection' || $sCustomColumn == 'selection_selected')
            {
                $aClassName = explode('\\', get_class($oEntity));
                $aReturn['data'][$iKey][] = '<div class="text-center"><input type="checkbox" value="'.$oEntity->getId().'" name="'.$aClassName[sizeof($aClassName)-1].'_selection[]"' . (($sCustomColumn == 'selection_selected') ? 'checked="checked"' : '') . '/></div>';
            }
        }
        if(array_key_exists('data', $aReturn) == false) {
            $aReturn['data'] = array();
        }

        return $aReturn;
    }
}