<?php
namespace DSJ\CMS\ServiceBundle\Manager\Content;

use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\ORM\EntityManager;
use Gedmo\Sluggable\Util\Urlizer;

class TranslationManager extends ContainerAware
{
    /**
     * @var EntityManager $oEm
     */
    private $oEm;

    private $bTranslations;
    private $sGoogleTranslateApi;
    private $oSession;
    private $sLocale;

    public function __construct($oRegistry, $aTranslations, $oSession, $sLocale) {
        $this->oEm = $oRegistry->getManager();
        $this->oSession = $oSession;
        $this->sLocale = $sLocale;
        $this->bTranslations = $aTranslations['enabled'];
        if($aTranslations['enabled'] === true) {
            $this->sGoogleTranslateApi = $aTranslations['google_api_key'];
        }
    }

    public function setSlug($oEntity, $sSluggableValue) {
        $oEntity->setSlug(Urlizer::urlize(Urlizer::utf8ToAscii($sSluggableValue, "-")));

        return $oEntity;
    }

    public function getLocale()
    {
        $sBackendLocale = $this->oSession->get('backend_locale');
        if($sBackendLocale !== null) {
            return $sBackendLocale;
        }
        return $this->sLocale;
    }

    public function setLocale($oEntity)
    {
        $sBackendLocale = $this->oSession->get('backend_locale');
        if($sBackendLocale !== null){
            $oEntity->setTranslatableLocale($sBackendLocale);
        } else {
            $oEntity->setTranslatableLocale($this->sLocale);
        }
        return $oEntity;
    }

    public function getLanguages() {
        $aLanguages=false;
        if($this->bTranslations == true) {
            $aLanguages = $this->oEm->getRepository('DSJCMSDBBundle:Content\Language')->findAll();
        }

        return $aLanguages;
    }

    public function translate($sText, $sSourceLanguage, $sTargetLanguage)
    {
        if($sText == "" || $sText == NULL) return "";

        $i = 0;
        $replacements = array();
        $matches = array(); unset($matches); $i=0;
        preg_match_all('/&%[a-zA-Z:\(\)_-]+%&/U', $sText, $matches);
        foreach($matches[0] as $match)
        {
            $sText = str_replace($match, '<span class="notranslate">'.$match.'</span>', $sText);
            $i++;
        }
        $matches2 = array(); unset($matches2);
        preg_match_all('/\%[a-zA-Z_-]+\%/U', $sText, $matches2);
        foreach($matches2[0] as $match2)
        {
            $sText = str_replace($match2, '<span class="notranslate">'.$match2.'</span>', $sText);
            $i++;
        }
        $matches3 = array(); unset($matches3); $i=0;
        preg_match_all('/%&[1-9:\(\)_-]+&%/', $sText, $matches3);
        foreach($matches3[0] as $match3)
        {
            $sText = str_replace($match3, '<span class="notranslate">'.$match3.'</span>', $sText);
            $i++;
        }
        print "translating: ".$sText."\n";

        $aPostfieldsData = array("key"=>$this->sGoogleTranslateApi, "q"=>$sText, "source"=>$sSourceLanguage, "target"=>$sTargetLanguage);
        $url = "https://www.googleapis.com/language/translate/v2";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $aPostfieldsData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET') );
        $data = curl_exec($ch);
        curl_close($ch);
        sleep(1);

        $translation = json_decode($data);
        $translation_text = $translation->data->translations[0]->translatedText;
        $list = get_html_translation_table(HTML_ENTITIES);

        unset($list['"']);
        unset($list['\'']);
        unset($list['<']);
        unset($list['>']);
        unset($list['&']);

        $search = array_keys($list);
        $values = array_values($list);
        $search = array_map('utf8_encode', $search);

        $str_in = $translation_text;
        $str_out = str_replace($search, $values, $str_in);
        $str_out = html_entity_decode(preg_replace('#<span class="notranslate">(.*?)</span>#', '$1', $str_out));

        if(sizeof($replacements)>0)
        {
            foreach($replacements as $replacement)
            {
                $key = $replacement["key"];
                $value = $replacement["value"];
                $str_out = str_replace($key, $value, $str_out);
            }
        }

        return $str_out;
    }
}