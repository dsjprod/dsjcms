<?php
namespace DSJ\CMS\ServiceBundle\Manager\Mail;

use DSJ\CMS\DBBundle\Entity\System\EmailLog;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Doctrine\ORM\EntityManager;

class MailingManager extends ContainerAware
{
    /**
     * @var EntityManager $oEm
     */
    private $oEm;

    /**
     * @var \Swift_Mailer $oMailer
     */
    private $oMailer;

    /**
     * @var TwigEngine $oTemplating
     */
    private $oTemplating;

    private $sWebsiteName;
    private $sHttpHost;
    private $sMailFrom;
    private $sBcc;


    public function __construct(
        $oRegistry,
        \Swift_Mailer $oMailer,
        TwigEngine $oTemplating,
        $sHttpHost,
        $sMailFrom,
        $sBcc,
        $sWebsiteName
    ) {
        $this->oEm = $oRegistry->getManager();
        $this->oMailer = $oMailer;
        $this->oTemplating = $oTemplating;
        $this->sWebsiteName = $sWebsiteName;
        $this->sHttpHost = $sHttpHost;
        $this->sMailFrom = $sMailFrom;
        $this->sBcc = $sBcc;
    }

    public function sendEmail($sSubject, $sBody, $aRecipients, $sCc=NULL, $aAttachments=NULL, $sBcc=NULL, $oEmail=NULL, $sReplyTo=NULL)
    {
        $sEmail = $this->oTemplating->render(
            'DSJCMSFrontendBackofficeBundle:Email:layout.html.twig',
            array(
                'httpHost' => $this->sHttpHost,
                'content' => $sBody,
                'subject' => $sSubject
            )
        );

        $sFrom = $this->sMailFrom;
        if(is_array($aRecipients)) {
            foreach($aRecipients as $sRecipient)
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject($sSubject)
                    ->setFrom(array($sFrom => $this->sWebsiteName))
                    ->setTo($sRecipient)
                    ->setBody($sEmail, 'text/html');

                if($sReplyTo !== NULL) {
                    $message->addReplyTo(trim($sReplyTo));
                }
                if($sCc !== NULL) {
                    $message->addCc(trim($sCc));
                }
                if($sBcc !== NULL) {
                    $message->addBcc(trim($sBcc));
                }

                if($aAttachments !== NULL) {
                    if(is_array($aAttachments)) {
                        foreach($aAttachments as $sAttachment) {
                            $message->attach(\Swift_Attachment::fromPath($sAttachment));
                        }
                    } else {
                        $message->attach(\Swift_Attachment::fromPath($aAttachments));
                    }
                }

                $this->oMailer->send($message);
                $this->setEmailLog($sFrom, $sRecipient, $sSubject, $sEmail, $aAttachments, $oEmail);
            }
        } else {
            $message = \Swift_Message::newInstance()
                ->setSubject($sSubject)
                ->setFrom(array($sFrom => $this->sWebsiteName))
                ->setTo($aRecipients)
                ->setBody($sEmail, 'text/html');

            if($sReplyTo !== NULL) {
                $message->addReplyTo(trim($sReplyTo));
            }
            if($sCc !== NULL) {
                $message->addCc(trim($sCc));
            }
            if($sBcc !== NULL) {
                $message->addBcc(trim($sBcc));
            }

            if($aAttachments !== NULL) {
                if(is_array($aAttachments)) {
                    foreach($aAttachments as $sAttachment) {
                        $message->attach(\Swift_Attachment::fromPath($sAttachment));
                    }
                } else {
                    $message->attach(\Swift_Attachment::fromPath($aAttachments));
                }
            }

            $this->oMailer->send($message);
            $this->setEmailLog($sFrom, $aRecipients, $sSubject, $sEmail, $aAttachments, $oEmail);
        }
    }

    private function setEmailLog($sFrom, $sTo, $sSubject, $sContent, $aAttachments=null, $oEmail=null)
    {
        /** @var EmailLog $oEmailLog */
        $oEmailLog = new EmailLog();
        $oEmailLog->setFrom($sFrom);
        $oEmailLog->setTo($sTo);
        $oEmailLog->setSubject(trim($sSubject));
        $oEmailLog->setContent(trim($sContent));
        if($aAttachments !== NULL) {
            $sAttachments = '';
            if(is_array($aAttachments)) {
                foreach ($aAttachments as $i => $sAttachment) {
                    $sAttachments .= ($i > 0 ? '\n' : '') . $sAttachment;
                }
            } else {
                $sAttachments = $aAttachments;
            }
            $oEmailLog->setAttachments($sAttachments);
        }
        $oEmailLog->setSent(new \DateTime('NOW'));
        $oEmailLog->setEmail($oEmail);

        $this->oEm->persist($oEmailLog);
        $this->oEm->flush();
    }
}