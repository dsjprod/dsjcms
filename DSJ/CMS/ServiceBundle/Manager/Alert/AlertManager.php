<?php
namespace DSJ\CMS\ServiceBundle\Manager\Alert;

use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\ORM\EntityManager;

class AlertManager extends ContainerAware
{
    /**
     * @var EntityManager $oEm
     */
    private $oEm;

    public function __construct($oRegistry) {
        $this->oEm = $oRegistry->getManager();
    }

    public function getAlert($sIdentifier)
    {
        $oAlert = $this->oEm->getRepository('DSJCMSDBBundle:System\Alert')->findOneByIdentifier($sIdentifier);
        return $oAlert;
    }
}