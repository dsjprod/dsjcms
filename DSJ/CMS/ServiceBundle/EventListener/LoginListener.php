<?php

namespace DSJ\CMS\ServiceBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Bundle\DoctrineBundle\Registry;

class LoginListener
{
    private $oSecurityContext;
    private $oEm;

    /**
     * Constructor
     *
     * @param SecurityContext $oSecurityContext
     * @param Registry        $oRegistry
     */
    public function __construct(SecurityContext $oSecurityContext, Registry $oRegistry)
    {
        $this->oSecurityContext = $oSecurityContext;
        $this->oEm              = $oRegistry->getEntityManager();
    }

    /**
     *
     * @param InteractiveLoginEvent $oEvent
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $oEvent)
    {
        if ($this->oSecurityContext->isGranted('IS_AUTHENTICATED_FULLY') || $this->oSecurityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if($this->oSecurityContext->getToken()->getUser() instanceof \DSJ\CMS\DBBundle\Entity\Account\User) {
                $oUser = $this->oEm->getRepository("DSJCMSDBBundle:Account\User")->find($this->oSecurityContext->getToken()->getUser()->getId());
                $oUser->setLastLogin($oUser->getCurrentLogin());
                $oUser->setCurrentLogin(new \DateTime());
                $this->oEm->flush();
            }
        }
    }
}