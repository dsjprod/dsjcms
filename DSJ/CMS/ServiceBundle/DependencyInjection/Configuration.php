<?php

namespace DSJ\CMS\ServiceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dsjcms');

        $rootNode
            ->children()
                ->scalarNode('website_name')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('http_host')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('mail_from')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('mail_merchant')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('mail_merchant_name')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('mail_bcc')
                    ->defaultValue(NULL)
                ->end()
                // backoffice parameters
                ->arrayNode('backoffice')
                    ->children()
                        // menu parameters
                        ->arrayNode('menu')
                            ->cannotBeEmpty()
                            ->children()
                                ->arrayNode('content')->prototype('scalar')->end()->end()
                                ->arrayNode('newsletter')->prototype('scalar')->end()->end()
                                ->arrayNode('settings')->prototype('scalar')->end()->end()
                            ->end()
                        ->end() // end menu
                        ->integerNode('show_amount')->defaultValue(50)->end()
                    ->end()
                ->end() // end backoffice
                // form parameters
                ->arrayNode('form')
                    ->cannotBeEmpty()
                    ->children()
                        ->arrayNode('page')->prototype('scalar')->end()->end()
                        ->arrayNode('pageRow')->prototype('scalar')->end()->end()
                        ->arrayNode('content')->prototype('scalar')->end()->end()
                        ->arrayNode('slide')->prototype('scalar')->end()->end()
                        ->arrayNode('email')->prototype('scalar')->end()->end()
                        ->arrayNode('alert')->prototype('scalar')->end()->end()
                        ->arrayNode('response')->prototype('scalar')->end()->end()
                        ->arrayNode('tag')->prototype('scalar')->end()->end()
                    ->end()
                ->end() // end form

                // translations parameters
                ->arrayNode('translations')
                    ->canBeEnabled()
                    ->children()
                        ->scalarNode('google_api_key')->end()
                    ->end()
                ->end() // end translations
            ->end()
        ;

        return $treeBuilder;
    }
}
