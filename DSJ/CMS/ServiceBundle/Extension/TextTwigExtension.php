<?php
namespace DSJ\CMS\ServiceBundle\Extension;

use Doctrine\ORM\EntityManager;

class TextTwigExtension extends \Twig_Extension
{
	private $oEm;
    private $aBackoffice;
    private $sWebsiteName;
    private $sHttpHost;
	
    public function __construct($oRegistry, $aBackoffice, $sWebsiteName, $sHttpHost) {
        $this->oEm = $oRegistry->getManager();
        $this->aBackoffice = $aBackoffice;
        $this->sWebsiteName = $sWebsiteName;
        $this->sHttpHost = $sHttpHost;
    }
    /**
     * Returns a list of filters.
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            'get_class' => new \Twig_Filter_Method($this, 'get_class'),
            'is_array' => new \Twig_Filter_Method($this, 'is_array'),
            'is_object' => new \Twig_Filter_Method($this, 'is_object'),
            'ucfirst' => new \Twig_Filter_Method($this, 'ucfirst'),
        );
    }

    public function getGlobals() {
        return array(
            'index_show_amount' => $this->aBackoffice['show_amount'],
            'website_name'      => $this->sWebsiteName,
            'http_host'         => $this->sHttpHost,
        );
    }

    public function get_class($value)
    {
        return get_class($value);
    }

    public function is_array($value)
    {
        return is_array($value);
    }

    public function is_object($value)
    {
        return is_object($value);
    }

    public function ucfirst($value)
    {
        return ucfirst($value);
    }

    public function getName()
    {
        return 'twig_extension';
    }
}

?>