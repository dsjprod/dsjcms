<?php
namespace DSJ\CMS\BackofficeBundle\Controller\Content;

use Doctrine\ORM\EntityManager;
use DSJ\CMS\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SequenceController
 *
 * @Route("/menu-volgorde")
 */
class SequenceController extends Controller
{
    /**
     * @Route("/", name="dsj_cms_sequence")
     * @Template()
     */
    public function indexAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Page')->findBy(array('parent' => NULL, 'menuItem' => true), array('sequence' => 'ASC'));

        return array(
            'menu'      => 'content',
            'submenu'   => 'sequence',
            'entities'  => $aEntities,
        );
    }
}