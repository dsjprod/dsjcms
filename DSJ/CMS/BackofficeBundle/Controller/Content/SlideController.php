<?php

namespace DSJ\CMS\BackofficeBundle\Controller\Content;

use DSJ\CMS\BackofficeBundle\Form\SlideType;
use DSJ\CMS\ServiceBundle\Manager\Backoffice\LayoutManager;
use Store24\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use DSJ\CMS\FrontendDBBundle\Entity\Content\Slide;

/**
 * Content\Slide controller.
 *
 * @Route("/slide")
 */
class SlideController extends Controller
{
    /**
     * Lists all Content\Slide entities.
     *
     * @Route("/", name="dsj_cms_slide")
     * @Template()
     */
    public function indexAction()
    {
        return array(
        	'menu' 		=> 'content',
        	'submenu'     => 'slide',
        );
    }

    /**
     * @Route("/fetch", name="dsj_cms_slide_fetch")
     */
    public function fetchAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $aColumns = $oRequest->request->get('columns');
        $aSearchColumns = array();
        $aFetchColumns = array();
        $i=0;
        foreach($aColumns as $aField) {
            if($aField['searchable'] !== 'false' && $aField['searchable'] !== false) {
                if (array_key_exists('column', $aField)) {
                    $aSearchColumns[$i] = $aField['column'];
                    $i++;
                }
            }
        }
        $iDisplayStart = $oRequest->request->get('start');
        $iDisplayLength = $oRequest->request->get('length');
        $aSearch = $oRequest->request->get('search', null);
        $sSearch = ($aSearch['value'] == '' ? NULL : $aSearch['value']);
        $aOrder = $oRequest->request->get('order', null);
        $aNewOrder = array();
        foreach($aOrder as $order) {
            $aNewOrder[$aColumns[intval($order['column'])]['column']] = $order['dir'];
        }

        if($sSearch == NULL) {
            $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Slide')->getFiltered($iDisplayLength, $iDisplayStart, array(), null, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Slide')->getCount();
        } else {
            $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Slide')->getFiltered($iDisplayLength, $iDisplayStart, $aSearchColumns, $sSearch, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Slide')->getFilteredCount($aSearchColumns, $sSearch, $aNewOrder);
        }

        $aReturn = array();

        /** @var LayoutManager $oBackofficeLayoutManager */
        $oBackofficeLayoutManager = $this->get('dsj.cms.managers.backoffice.layout');
        $aReturn = $oBackofficeLayoutManager->getFetchLayout($aEntities, $aColumns, 'dsj_cms_slide_edit', 'dsj_cms_slide_delete');

        $aReturn["recordsTotal"] = $iCount;
        $aReturn["recordsFiltered"] = $iCount;

        return new Response(json_encode($aReturn));
    }

    /**
     * Displays a form to create a new Content\Slide entity.
     *
     * @Route("/new", name="dsj_cms_slide_new")
     * @Template()
     */
    public function newAction()
    {
        $oRequest = $this->getRequest();
        $oEntity = new Slide();
        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('slide', $aForms) ? $aForms['slide'] : array());
        $oForm   = $this->createForm(new SlideType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);
            if ($oForm->isValid()) {
                $oEm = $this->getDoctrine()->getManager();
                $oHighestSequence = $oEm->getRepository('DSJCMSDBBundle:Content\Slide')->findOneBy(array(), array('sequence' => 'DESC'));
                if(NULL === $oHighestSequence)
                    $oEntity->setSequence(0);
                else
                    $oEntity->setSequence($oHighestSequence->getSequence()+1);

                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_slide_edit', array('id' => $oEntity->getId())));
            }
        }

        return array(
            'entity'    => $oEntity,
            'form'      => $oForm->createView(),
            'languages' => $aLanguages,
        	'menu' 		=> 'content',
        	'submenu'   => 'slide',
        );
    }

    /**
     * Displays a form to edit an existing Content\Slide entity.
     *
     * @Route("/{id}/edit", name="dsj_cms_slide_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $oRequest = $this->getRequest();
        $oEm = $this->getDoctrine()->getManager();

        $oEntity = $oEm->getRepository('DSJCMSDBBundle:Content\Slide')->find($id);

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find Content\Slide entity.');
        }

        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('slide', $aForms) ? $aForms['slide'] : array());
        $oForm = $this->createForm(new SlideType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);

            if ($oForm->isValid()) {
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_slide_edit', array('id' => $id)));
            }
        }

        return array(
            'entity'      => $oEntity,
            'form'   => $oForm->createView(),
            'languages' => $aLanguages,
        	'menu' 		=> 'content',
        	'submenu'     => 'slide',
        );
    }

    /**
     * Deletes a Content\Slide entity.
     *
     * @Route("/{id}/delete", name="dsj_cms_slide_delete")
     */
    public function deleteAction($id)
    {
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $oEm = $this->getDoctrine()->getManager();
        $oEntity = $oEm->getRepository('DSJCMSDBBundle:Content\Slide')->find($id);

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find Content\Slide entity.');
        }

        $oEm->remove($oEntity);
        $oEm->flush();

        $oSession->getFlashBag()->add('success', 'Slide verwijderd!');

        return $this->redirect($this->generateUrl('dsj_cms_slide'));
    }
}
