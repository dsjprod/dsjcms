<?php

namespace DSJ\CMS\BackofficeBundle\Controller\Content;

use DSJ\CMS\BackofficeBundle\Form\PagePageRowType;
use DSJ\CMS\DBBundle\Entity\Content\PageRow;
use DSJ\CMS\FrontendDBBundle\Entity\Content\Page;
use Store24\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * Content\PageContent controller.
 *
 * @Route("/page-content")
 */
class PageContentController extends Controller
{
    /**
     * @Route("/edit/{page}", name="dsj_cms_page_content_form")
     * @Template()
     */
    public function formAction($page)
    {
        $oRequest = $this->getRequest();
        $oEm = $this->getDoctrine()->getManager();
        /** @var Page $oEntity */
        $oEntity = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Page')->find($page);

        $originalRows = array();
        $originalContent = array();
        if($oEntity->getPageRows()){
            /** @var PageRow $oPageRow */
            foreach ($oEntity->getPageRows() as $oPageRow) {
                $aContent = array();
                foreach($oPageRow->getPageContent() as $oContent) {
                    $aContent[] = $oContent;
                }
                $originalRows[] = $oPageRow;
                $originalContent[$oPageRow->getId()] = $aContent;
            }
        }

        $aForms = $this->container->getParameter('dsj.cms.form');
        $oForm = $this->createForm(new PagePageRowType(), $oEntity, array(
            'em'        => $oEm,
            'forms'    => $aForms,
        ));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);
        $sBackendLocale = $oTranslationManager->getLocale();

        $aTypes = $oEm->getRepository('DSJCMSDBBundle:Content\ContentType')->findAll();

        if($oRequest->getMethod() == "POST") {
            $oForm->handleRequest($oRequest);
            if ($oForm->isValid()) {
                $oEntity = $oTranslationManager->setLocale($oEntity);
                if ($oEntity->getPageRows()) {
                    foreach ($oEntity->getPageRows() as $oPageRow) {
                        if (($key = array_search($oPageRow, $originalRows)) !== false) {
                            unset($originalRows[$key]);
                        }
                        if ($oPageRow->getPage() != $oEntity) {
                            $oPageRow->setPage($oEntity);
                        }

                        foreach ($oPageRow->getPageContent() as $oContent) {
                            $oTranslationManager->setSlug($oContent, $oContent->getTitle());
                            if (($key = array_search($oContent, $originalContent[$oPageRow->getId()])) !== false) {
                                unset($originalContent[$oPageRow->getId()][$key]);
                            }
                            if ($sBackendLocale !== null) {
                                $oContent->setTranslatableLocale($sBackendLocale);
                            } else {
                                $oContent->setTranslatableLocale('nl');
                            }
                            if ($oContent->getPageRow() != $oPageRow) {
                                $oContent->setPageRow($oPageRow);
                            }
                        }
                        if ($originalContent[$oPageRow->getId()]) {
                            foreach ($originalContent[$oPageRow->getId()] as $oContent) {
                                $oEm->remove($oContent);
                            }
                        }
                    }
                }

                foreach ($originalRows as $oPageRow) {
                    foreach ($oPageRow->getPageContent() as $oContent) {
                        $oEm->remove($oContent);
                    }
                    $oEm->remove($oPageRow);
                }

                if ($oForm->get('addRow')->getData() == 1) {
                    $oPageRow = new PageRow();
                    $oHighestSequence = $oEm->getRepository('DSJCMSDBBundle:Content\PageRow')->findOneBy(array(), array('sequence' => 'desc'));
                    if($oHighestSequence !== NULL && $oHighestSequence->getSequence() !== NULL) {
                        $oPageRow->setSequence($oHighestSequence->getSequence()+1);
                    } else {
                        $oPageRow->setSequence(0);
                    }
                    $oPageRow->setPage($oEntity);
                    $oEntity->addPageRow($oPageRow);
                    $oEm->persist($oPageRow);
                }
                $oEm->persist($oEntity);
                $oEm->flush();
                $oEm->refresh($oEntity);
            }
            $oForm = $this->createForm(new PagePageRowType(), $oEntity, array(
                'em'        => $oEm,
                'forms'    => $aForms,
            ));

            return new Response(json_encode(
                array(
                    'response'  => $this->renderView('DSJCMSBackofficeBundle:Content\PageContent:form.html.twig', array(
                        'entity'        => $oEntity,
                        'contenttypes'  => $aTypes,
                        'form'          => $oForm->createView(),
                        'languages'     => $aLanguages,
                    ))
                )
            ));
        }

        return array(
            'entity'        => $oEntity,
            'contenttypes'  => $aTypes,
            'form'          => $oForm->createView(),
            'languages'     => $aLanguages,
        );
    }
}
