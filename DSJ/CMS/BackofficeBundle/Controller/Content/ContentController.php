<?php

namespace DSJ\CMS\BackofficeBundle\Controller\Content;

use DSJ\CMS\BackofficeBundle\Form\ContentType;
use DSJ\CMS\ServiceBundle\Manager\Backoffice\LayoutManager;
use Store24\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use DSJ\CMS\FrontendDBBundle\Entity\Content\Content;

/**
 * Content\Content controller.
 *
 * @Route("/content")
 */
class ContentController extends Controller
{
    /**
     * Lists all Content\Content entities.
     *
     * @Route("/", name="dsj_cms_content")
     * @Template()
     */
    public function indexAction()
    {
        return array(
        	'menu' 		=> 'content',
        	'submenu'     => 'content',
        );
    }

    /**
     * @Route("/fetch", name="dsj_cms_content_fetch")
     */
    public function fetchAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $aColumns = $oRequest->request->get('columns');
        $aSearchColumns = array();
        $aFetchColumns = array();
        $i=0;
        foreach($aColumns as $aField) {
            if($aField['searchable'] !== 'false' && $aField['searchable'] !== false) {
                if (array_key_exists('column', $aField)) {
                    $aSearchColumns[$i] = $aField['column'];
                    $i++;
                }
            }
        }
        $iDisplayStart = $oRequest->request->get('start');
        $iDisplayLength = $oRequest->request->get('length');
        $aSearch = $oRequest->request->get('search', null);
        $sSearch = ($aSearch['value'] == '' ? NULL : $aSearch['value']);
        $aOrder = $oRequest->request->get('order', null);
        $aNewOrder = array();
        foreach($aOrder as $order) {
            $aNewOrder[$aColumns[intval($order['column'])]['column']] = $order['dir'];
        }

        if($sSearch == NULL) {
            $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Content')->getFiltered($iDisplayLength, $iDisplayStart, array(), null, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Content')->getCount();
        } else {
            $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Content')->getFiltered($iDisplayLength, $iDisplayStart, $aSearchColumns, $sSearch, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Content')->getFilteredCount($aSearchColumns, $sSearch, $aNewOrder);
        }

        $aReturn = array();

        /** @var LayoutManager $oBackofficeLayoutManager */
        $oBackofficeLayoutManager = $this->get('dsj.cms.managers.backoffice.layout');
        $aReturn = $oBackofficeLayoutManager->getFetchLayout($aEntities, $aColumns, 'dsj_cms_content_edit', 'dsj_cms_content_delete');

        $aReturn["recordsTotal"] = $iCount;
        $aReturn["recordsFiltered"] = $iCount;

        return new Response(json_encode($aReturn));
    }

    /**
     * Displays a form to create a new Content\Content entity.
     *
     * @Route("/new", name="dsj_cms_content_new")
     * @Template()
     */
    public function newAction()
    {
        $oRequest = $this->getRequest();
        $oEntity = new Content();
        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('content', $aForms) ? $aForms['content'] : array());
        $oForm   = $this->createForm(new ContentType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);
            if ($oForm->isValid()) {
                $oEm = $this->getDoctrine()->getManager();
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_content_edit', array('id' => $oEntity->getId())));
            }
        }

        return array(
            'entity'    => $oEntity,
            'form'      => $oForm->createView(),
            'languages' => $aLanguages,
        	'menu' 		=> 'content',
        	'submenu'   => 'content',
        );
    }

    /**
     * Displays a form to edit an existing Content\Content entity.
     *
     * @Route("/{id}/edit", name="dsj_cms_content_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $oRequest = $this->getRequest();
        $oEm = $this->getDoctrine()->getManager();

        $oEntity = $oEm->getRepository('DSJCMSDBBundle:Content\Content')->find($id);

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find Content\Content entity.');
        }

        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('content', $aForms) ? $aForms['content'] : array());
        $oForm = $this->createForm(new ContentType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);

            if ($oForm->isValid()) {
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_content_edit', array('id' => $id)));
            }
        }

        return array(
            'entity'        => $oEntity,
            'form'          => $oForm->createView(),
            'languages'     => $aLanguages,
        	'menu' 		    => 'content',
        	'submenu'       => 'content',
        );
    }

    /**
     * Deletes a Content\Content entity.
     *
     * @Route("/{id}/delete", name="dsj_cms_content_delete")
     */
    public function deleteAction($id)
    {
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $oEm = $this->getDoctrine()->getManager();
        $oEntity = $oEm->getRepository('DSJCMSDBBundle:Content\Content')->find($id);

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find Content\Content entity.');
        }

        $oEm->remove($oEntity);
        $oEm->flush();

        $oSession->getFlashBag()->add('success', 'Inhoud verwijderd!');

        return $this->redirect($this->generateUrl('dsj_cms_content'));
    }
}
