<?php
namespace DSJ\CMS\BackofficeBundle\Controller\Content;

use Doctrine\ORM\EntityManager;
use DSJ\CMS\BackofficeBundle\Form\PageType;
use DSJ\CMS\DBBundle\Entity\Content\PageRow;
use DSJ\CMS\FrontendDBBundle\Entity\Content\Page;
use DSJ\CMS\ServiceBundle\Manager\Backoffice\LayoutManager;
use DSJ\CMS\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PageController
 *
 * @Route("/pagina")
 */
class PageController extends Controller
{
    /**
     * @Route("/", name="dsj_cms_page")
     * @Template()
     */
    public function indexAction()
    {
        return array(
            'menu'  => 'content',
            'submenu'  => 'page',
        );
    }

    /**
     * @Route("/fetch", name="dsj_cms_page_fetch")
     */
    public function fetchAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $aColumns = $oRequest->request->get('columns');
        $aSearchColumns = array();
        $aFetchColumns = array();
        $i=0;
        foreach($aColumns as $aField) {
            if($aField['searchable'] !== 'false' && $aField['searchable'] !== false) {
                if (array_key_exists('column', $aField)) {
                    $aSearchColumns[$i] = $aField['column'];
                    $i++;
                }
            }
        }
        $iDisplayStart = $oRequest->request->get('start');
        $iDisplayLength = $oRequest->request->get('length');
        $aSearch = $oRequest->request->get('search', null);
        $sSearch = ($aSearch['value'] == '' ? NULL : $aSearch['value']);
        $aOrder = $oRequest->request->get('order', null);
        $aNewOrder = array();
        foreach($aOrder as $order) {
            $aNewOrder[$aColumns[intval($order['column'])]['column']] = $order['dir'];
        }

        if($sSearch == NULL) {
            $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Page')->getFiltered($iDisplayLength, $iDisplayStart, array(), null, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Page')->getCount();
        } else {
            $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Page')->getFiltered($iDisplayLength, $iDisplayStart, $aSearchColumns, $sSearch, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Page')->getFilteredCount($aSearchColumns, $sSearch, $aNewOrder);
        }

        $aReturn = array();

        /** @var LayoutManager $oBackofficeLayoutManager */
        $oBackofficeLayoutManager = $this->get('dsj.cms.managers.backoffice.layout');
        $aReturn = $oBackofficeLayoutManager->getFetchLayout($aEntities, $aColumns, 'dsj_cms_page_edit');

        $aReturn["recordsTotal"] = $iCount;
        $aReturn["recordsFiltered"] = $iCount;

        return new Response(json_encode($aReturn));
    }

    /**
     * @Route("/new", name="dsj_cms_page_new")
     * @Template()
     */
    public function newAction()
    {
        $oRequest = $this->getRequest();
        $oEm = $this->getDoctrine()->getManager();
        /** @var Page $oEntity */
        $oEntity = new Page();

        $aForms = $this->container->getParameter('dsj.cms.form');
        $oForm = $this->createForm(new PageType(), $oEntity, array(
            'em'        => $oEm,
            'forms'    => $aForms,
        ));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->isMethod("POST")) {
            $oForm->handleRequest($oRequest);

            if ($oForm->isValid()) {
                $oTranslationManager->setSlug($oEntity, $oEntity->getPageTitle());
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_page_edit', array('id' => $oEntity->getId())));
            }
        }

        $aTypes = $oEm->getRepository('DSJCMSDBBundle:Content\ContentType')->findAll();

        return array(
            'contenttypes' => $aTypes,
            'form'      => $oForm->createView(),
            'languages' => $aLanguages,
            'menu'      => 'content',
            'submenu'   => 'page',
        );
    }

    /**
     * @Route("/edit/{id}", name="dsj_cms_page_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $oEm       = $this->getDoctrine()->getManager();
        /** @var Page $oEntity */
        $oEntity = $oEm->getRepository('DSJCMSFrontendDBBundle:Content\Page')->find($id);

        $aForms = $this->container->getParameter('dsj.cms.form');
        $oForm = $this->createForm(new PageType(), $oEntity, array(
            'em'        => $oEm,
            'forms'    => $aForms,
        ));

        $oRequest = $this->getRequest();
        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);
        $sBackendLocale = $oTranslationManager->getLocale();

        if($oRequest->isMethod("POST")) {
            $oForm->handleRequest($oRequest);
            if ($oForm->isValid()) {
                $oTranslationManager->setSlug($oEntity, $oEntity->getPageTitle());
                $oEm->persist($oEntity);
                $oEm->flush();

                $oEm->refresh($oEntity);

                return $this->redirect($this->generateUrl('dsj_cms_page_edit', array('id' => $oEntity->getId())), 301);
            }
        }
        
        $aTypes = $oEm->getRepository('DSJCMSDBBundle:Content\ContentType')->findAll();
        $aLanguages = $oEm->getRepository('DSJCMSDBBundle:Content\Language')->findAll();
        return array(
            'menu'  => 'content',
            'submenu'  => 'page',
            'contenttypes' => $aTypes,
            'languages' => $aLanguages,
            'entity'      => $oEntity,
            'form'      => $oForm->createView(),
        );
    }
}