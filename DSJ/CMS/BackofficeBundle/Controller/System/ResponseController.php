<?php

namespace DSJ\CMS\BackofficeBundle\Controller\System;

use DSJ\CMS\BackofficeBundle\Form\ResponseType;
use DSJ\CMS\ServiceBundle\Manager\Backoffice\LayoutManager;
use DSJ\CMS\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use DSJ\CMS\FrontendDBBundle\Entity\System\Response as CMSResponse;

/**
 * System\Response controller.
 *
 * @Route("/reactie")
 */
class ResponseController extends Controller
{
    /**
     * Lists all System\Response entities.
     *
     * @Route("/", name="dsj_cms_response")
     * @Template()
     */
    public function indexAction()
    {
        return array(
        	'menu' 		=> 'content',
        	'submenu'     => 'response',
        );
    }

    /**
     * @Route("/fetch", name="dsj_cms_response_fetch")
     */
    public function fetchAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $aColumns = $oRequest->request->get('columns');
        $aSearchColumns = array();
        $aFetchColumns = array();
        $i=0;
        foreach($aColumns as $aField) {
            if($aField['searchable'] !== 'false' && $aField['searchable'] !== false) {
                if (array_key_exists('column', $aField)) {
                    $aSearchColumns[$i] = $aField['column'];
                    $i++;
                }
            }
        }
        $iDisplayStart = $oRequest->request->get('start');
        $iDisplayLength = $oRequest->request->get('length');
        $aSearch = $oRequest->request->get('search', null);
        $sSearch = ($aSearch['value'] == '' ? NULL : $aSearch['value']);
        $aOrder = $oRequest->request->get('order', null);
        $aNewOrder = array();
        foreach($aOrder as $order) {
            $aNewOrder[$aColumns[intval($order['column'])]['column']] = $order['dir'];
        }

        if($sSearch == NULL) {
            $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:System\Response')->getFiltered($iDisplayLength, $iDisplayStart, array(), null, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSFrontendDBBundle:System\Response')->getCount();
        } else {
            $aEntities = $oEm->getRepository('DSJCMSFrontendDBBundle:System\Response')->getFiltered($iDisplayLength, $iDisplayStart, $aSearchColumns, $sSearch, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSFrontendDBBundle:System\Response')->getFilteredCount($aSearchColumns, $sSearch, $aNewOrder);
        }

        $aReturn = array();

        /** @var LayoutManager $oBackofficeLayoutManager */
        $oBackofficeLayoutManager = $this->get('dsj.cms.managers.backoffice.layout');
        $aReturn = $oBackofficeLayoutManager->getFetchLayout($aEntities, $aColumns, 'dsj_cms_response_edit', 'dsj_cms_response_delete');

        $aReturn["recordsTotal"] = $iCount;
        $aReturn["recordsFiltered"] = $iCount;

        return new Response(json_encode($aReturn));
    }

    /**
     * Displays a form to create a new System\Response entity.
     *
     * @Route("/new", name="dsj_cms_response_new")
     * @Template()
     */
    public function newAction()
    {
        $oRequest = $this->getRequest();
        $oEntity = new CMSResponse();
        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('response', $aForms) ? $aForms['response'] : array());
        $oForm   = $this->createForm(new ResponseType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);
            if ($oForm->isValid()) {
                $oEm = $this->getDoctrine()->getManager();
                $oTranslationManager->setSlug($oEntity, $oEntity->getTitle());
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_response_edit', array('id' => $oEntity->getId())));
            }
        }

        return array(
            'entity'    => $oEntity,
            'form'      => $oForm->createView(),
            'languages' => $aLanguages,
        	'menu' 		=> 'content',
        	'submenu'   => 'response',
        );
    }

    /**
     * Displays a form to edit an existing System\Response entity.
     *
     * @Route("/{id}/edit", name="dsj_cms_response_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $oRequest = $this->getRequest();
        $oEm = $this->getDoctrine()->getManager();

        $oEntity = $oEm->getRepository('DSJCMSFrontendDBBundle:System\Response')->find($id);

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find System\Response entity.');
        }

        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('response', $aForms) ? $aForms['response'] : array());
        $oForm = $this->createForm(new ResponseType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);

            if ($oForm->isValid()) {
                $oTranslationManager->setSlug($oEntity, $oEntity->getTitle());
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_response_edit', array('id' => $id)));
            }
        }

        return array(
            'entity'      => $oEntity,
            'form'   => $oForm->createView(),
            'languages' => $aLanguages,
        	'menu' 		=> 'content',
        	'submenu'     => 'response',
        );
    }

    /**
     * Deletes a System\Response entity.
     *
     * @Route("/{id}/delete", name="dsj_cms_response_delete")
     */
    public function deleteAction($id)
    {
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $oEm = $this->getDoctrine()->getManager();
        $oEntity = $oEm->getRepository('DSJCMSFrontendDBBundle:System\Response')->find($id);

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find System\Response entity.');
        }

        $oEm->remove($oEntity);
        $oEm->flush();

        $oSession->getFlashBag()->add('success', 'Reactie verwijderd!');

        return $this->redirect($this->generateUrl('dsj_cms_response'));
    }
}
