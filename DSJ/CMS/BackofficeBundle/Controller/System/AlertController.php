<?php

namespace DSJ\CMS\BackofficeBundle\Controller\System;

use DSJ\CMS\BackofficeBundle\Form\AlertType;
use DSJ\CMS\DBBundle\Entity\System\Alert;
use DSJ\CMS\ServiceBundle\Manager\Backoffice\LayoutManager;
use DSJ\CMS\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * System\Alert controller.
 *
 * @Route("/meldingen")
 */
class AlertController extends Controller
{
    /**
     * Lists all System\Alert entities.
     *
     * @Route("/", name="dsj_cms_alert")
     * @Template()
     */
    public function indexAction()
    {
        return array(
        	'menu' 		=> 'content',
        	'submenu'     => 'alert',
        );
    }

    /**
     * @Route("/fetch", name="dsj_cms_alert_fetch")
     */
    public function fetchAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $aColumns = $oRequest->request->get('columns');
        $aSearchColumns = array();
        $aFetchColumns = array();
        $i=0;
        foreach($aColumns as $aField) {
            if($aField['searchable'] !== 'false' && $aField['searchable'] !== false) {
                if (array_key_exists('column', $aField)) {
                    $aSearchColumns[$i] = $aField['column'];
                    $i++;
                }
            }
        }
        $iDisplayStart = $oRequest->request->get('start');
        $iDisplayLength = $oRequest->request->get('length');
        $aSearch = $oRequest->request->get('search', null);
        $sSearch = ($aSearch['value'] == '' ? NULL : $aSearch['value']);
        $aOrder = $oRequest->request->get('order', null);
        $aNewOrder = array();
        foreach($aOrder as $order) {
            $aNewOrder[$aColumns[intval($order['column'])]['column']] = $order['dir'];
        }

        if($sSearch == NULL) {
            $aEntities = $oEm->getRepository('DSJCMSDBBundle:System\Alert')->getFiltered($iDisplayLength, $iDisplayStart, array(), null, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSDBBundle:System\Alert')->getCount();
        } else {
            $aEntities = $oEm->getRepository('DSJCMSDBBundle:System\Alert')->getFiltered($iDisplayLength, $iDisplayStart, $aSearchColumns, $sSearch, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSDBBundle:System\Alert')->getFilteredCount($aSearchColumns, $sSearch, $aNewOrder);
        }

        $aReturn = array();

        /** @var LayoutManager $oBackofficeLayoutManager */
        $oBackofficeLayoutManager = $this->get('dsj.cms.managers.backoffice.layout');
        $aReturn = $oBackofficeLayoutManager->getFetchLayout($aEntities, $aColumns, 'dsj_cms_alert_edit');

        $aReturn["recordsTotal"] = $iCount;
        $aReturn["recordsFiltered"] = $iCount;

        return new Response(json_encode($aReturn));
    }

    /**
     * Displays a form to edit an existing System\Alert entity.
     *
     * @Route("/{id}/edit", name="dsj_cms_alert_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $oRequest = $this->getRequest();
        $oEm = $this->getDoctrine()->getManager();

        $oEntity = $oEm->getRepository('DSJCMSDBBundle:System\Alert')->find($id);

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find System\Alert entity.');
        }

        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('alert', $aForms) ? $aForms['alert'] : array());
        $oForm = $this->createForm(new AlertType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);

            if ($oForm->isValid()) {
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_alert_edit', array('id' => $id)));
            }
        }

        return array(
            'entity'      => $oEntity,
            'form'   => $oForm->createView(),
            'languages' => $aLanguages,
        	'menu' 		=> 'content',
        	'submenu'     => 'alert',
        );
    }
}
