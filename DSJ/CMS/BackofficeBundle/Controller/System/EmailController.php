<?php

namespace DSJ\CMS\BackofficeBundle\Controller\System;

use DSJ\CMS\BackofficeBundle\Form\EmailType;
use DSJ\CMS\ServiceBundle\Manager\Backoffice\LayoutManager;
use DSJ\CMS\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use DSJ\CMS\DBBundle\Entity\System\Email;

/**
 * Content\Email controller.
 *
 * @Route("/email")
 */
class EmailController extends Controller
{
    /**
     * Lists all System\Email entities.
     *
     * @Route("/", name="dsj_cms_email")
     * @Template()
     */
    public function indexAction()
    {
        return array(
        	'menu' 		=> 'content',
        	'submenu'     => 'email',
        );
    }

    /**
     * @Route("/fetch", name="dsj_cms_email_fetch")
     */
    public function fetchAction()
    {
        $oEm = $this->getDoctrine()->getManager();
        $oRequest = $this->getRequest();
        $oSession = $oRequest->getSession();

        $aColumns = $oRequest->request->get('columns');
        $aSearchColumns = array();
        $aFetchColumns = array();
        $i=0;
        foreach($aColumns as $aField) {
            if($aField['searchable'] !== 'false' && $aField['searchable'] !== false) {
                if (array_key_exists('column', $aField)) {
                    $aSearchColumns[$i] = $aField['column'];
                    $i++;
                }
            }
        }
        $iDisplayStart = $oRequest->request->get('start');
        $iDisplayLength = $oRequest->request->get('length');
        $aSearch = $oRequest->request->get('search', null);
        $sSearch = ($aSearch['value'] == '' ? NULL : $aSearch['value']);
        $aOrder = $oRequest->request->get('order', null);
        $aNewOrder = array();
        foreach($aOrder as $order) {
            $aNewOrder[$aColumns[intval($order['column'])]['column']] = $order['dir'];
        }

        if($sSearch == NULL) {
            $aEntities = $oEm->getRepository('DSJCMSDBBundle:System\Email')->getFiltered($iDisplayLength, $iDisplayStart, array(), null, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSDBBundle:System\Email')->getCount();
        } else {
            $aEntities = $oEm->getRepository('DSJCMSDBBundle:System\Email')->getFiltered($iDisplayLength, $iDisplayStart, $aSearchColumns, $sSearch, $aNewOrder);
            $iCount = $oEm->getRepository('DSJCMSDBBundle:System\Email')->getFilteredCount($aSearchColumns, $sSearch, $aNewOrder);
        }

        $aReturn = array();

        /** @var LayoutManager $oBackofficeLayoutManager */
        $oBackofficeLayoutManager = $this->get('dsj.cms.managers.backoffice.layout');
        $aReturn = $oBackofficeLayoutManager->getFetchLayout($aEntities, $aColumns, 'dsj_cms_email_edit');

        $aReturn["recordsTotal"] = $iCount;
        $aReturn["recordsFiltered"] = $iCount;

        return new Response(json_encode($aReturn));
    }

    /**
     * Displays a form to create a new System\Email entity.
     *
     * @Route("/new", name="dsj_cms_email_new")
     * @Template()
     */
    public function newAction()
    {
        $oRequest = $this->getRequest();
        $oEntity = new Email();
        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('email', $aForms) ? $aForms['email'] : array());
        $oForm   = $this->createForm(new EmailType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);
            if ($oForm->isValid()) {
                $oEm = $this->getDoctrine()->getManager();
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_email_edit', array('id' => $oEntity->getId())));
            }
        }

        return array(
            'entity'    => $oEntity,
            'form'      => $oForm->createView(),
            'languages' => $aLanguages,
        	'menu' 		=> 'content',
        	'submenu'   => 'email',
        );
    }

    /**
     * Displays a form to edit an existing System\Email entity.
     *
     * @Route("/{id}/edit", name="dsj_cms_email_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $oRequest = $this->getRequest();
        $oEm = $this->getDoctrine()->getManager();

        $oEntity = $oEm->getRepository('DSJCMSDBBundle:System\Email')->find($id);

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find System\Email entity.');
        }

        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('email', $aForms) ? $aForms['email'] : array());
        $oForm = $this->createForm(new EmailType(), $oEntity, array('fields' => $aFields));

        /** @var TranslationManager $oTranslationManager */
        $oTranslationManager = $this->get('dsj.cms.managers.translation');
        $aLanguages = $oTranslationManager->getLanguages();
        $oEntity = $oTranslationManager->setLocale($oEntity);

        if($oRequest->getMethod() === "POST") {
            $oForm->handleRequest($oRequest);

            if ($oForm->isValid()) {
                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_email_edit', array('id' => $id)));
            }
        }

        return array(
            'entity'      => $oEntity,
            'form'   => $oForm->createView(),
            'languages' => $aLanguages,
        	'menu' 		=> 'content',
        	'submenu'     => 'email',
        );
    }
}
