<?php

namespace DSJ\CMS\BackofficeBundle\Controller;

use DSJ\CMS\DBBundle\Entity\Content\ContentType;
use DSJ\CMS\DBBundle\Entity\System\Color;
use DSJ\CMS\ServiceBundle\Manager\Content\TranslationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

use Vich\UploaderBundle\Mapping\PropertyMapping;

/**
 * @Route("/api")
 */
class ApiController extends Controller
{
    /**
     * @Route("/sessionstatus", name="sessionStatus")
     */
    public function sessionStatusAction()
    {
    	$oRequest = $this->getRequest();
    	$oSession = $oRequest->getSession();
    	$oUser = $this->get('security.context')->getToken()->getUser();

    	$aResponse = array();
    	if($oUser !== NULL) {
    		$roles = $oUser->getRoles();
    		foreach($roles as $role){
    			$aResponse[] = $role;
    			$cookie = new Cookie('role', $role, strtotime('+2 hour'));
    			$cookieLastLogin = new Cookie('lastLogin', sha1($oUser->getLastLogin()->format('Y-m-d H:i:s')), strtotime('+2 hour'));
    			$cookieUserId = new Cookie('userID', $oUser->getId(), strtotime('+2 hour'));
    		}

	    	$oResponse = new Response(json_encode($aResponse));
	    	$oResponse->headers->setCookie($cookie);
	    	$oResponse->headers->setCookie($cookieLastLogin);
	    	$oResponse->headers->setCookie($cookieUserId);
	    	return $oResponse;
    	}
    	return new Response(json_encode(false));
    }

    /**
     * @Route("/sessionCheck/{id}/{date}", name="sessionCheck")
     */
    public function sessionCheckAction($id, $date)
    {
    	$oEm    = $this->getDoctrine()->getManager();
    	$oUser  = $oEm->getRepository('DSJCMSDBBundle:Account\User')->find($id);
    	$aResponse = array();

    	if($oUser){
    		if($date === sha1($oUser->getLastLogin()->format('Y-m-d H:i:s'))){
    			$aResponse['allowed'] = true;
    		} else {
    			$aResponse['allowed'] = false;
    		}
    	}

    	$oResponse = new Response(json_encode($aResponse));
    	return $oResponse;
    }

	/**
	 * Autocomplete for entities
	 *
	 * @Route("/autocomplete", name="dsj_cms_api_autocomplete")
	 * @Template()
	 */
	public function autocompleteAction()
	{
		$oRequest = $this->getRequest();
		$sObjectType = $oRequest->get('object', null);
		$sSearch = $oRequest->get('search', null);
		$sMethod = $oRequest->get('method', null);

		if($sObjectType === null || $sSearch === null) {
			return new Response("Incorrect request, missing parameters", 500);
		}
		if($sMethod == '') {
			$sMethod=null;
		}

		$em = $this->getDoctrine()->getManager();

		$oEntityRepository = null;
		if(stristr($sObjectType, '\\\\') !== false) {
			$sObjectType = str_replace('\\\\', '\\', $sObjectType);
		}

		switch($sObjectType) {
			case "page":
				if(class_exists('\DSJ\CMS\FrontendDBBundle\Entity\Content\Page'))
					$oEntityRepository = $em->getRepository('DSJCMSFrontendDBBundle:Content\Page');
				else
					$oEntityRepository = $em->getRepository('DSJCMSDBBundle:Content\Page');
				break;
			default:
				$oEntityRepository = $em->getRepository($sObjectType);
				break;
		}

		$aResponse = array();

		if($oEntityRepository === null) {
			return new Response("Incorrect parameters", 500);
		} else {
			/** @var TranslationManager $oTranslationManager */
			$oTranslationManager = $this->get('dsj.cms.managers.translation');

			if($sMethod === NULL) {
				$aEntities = $oEntityRepository->autocompleteSearch($sSearch, $oTranslationManager->getLocale());
			} else {
				$aEntities = call_user_func(array($oEntityRepository, $sMethod), $sSearch);
			}

			foreach($aEntities as $oEntity) {
				$aResponseItem = array();
				$aResponseItem["id"] = $oEntity->getId();
				if($sObjectType === 'page') {
					$aResponseItem["name"] = $oEntity->getTitle() .' (id: '.$oEntity->getId().')';
                    $aResponseItem["value"] = $aResponseItem["name"];
				} else {
					$aResponseItem["name"] = (string)$oEntity;
					$aResponseItem["value"] = $aResponseItem["name"];
				}
				$aResponse[] = $aResponseItem;
			}
		}

		$oResponse = new Response(json_encode($aResponse));
		$oResponse->setExpires(new \DateTime());
		return $oResponse;
	}

	/**
	 * Autocomplete for entities
	 *
	 * @Route("/autocomplete/remove", name="admin_api_autocomplete_remove")
	 * @Template()
	 */
	public function autocompleteRemoveAction()
	{
		$oRequest = $this->getRequest();
		$sObjectType = $oRequest->get('object', null);
		$iId = $oRequest->get('autocomplete_id', null);

		if($sObjectType === null || $iId === null) {
			return new Response("Incorrect request, missing parameters", 500);
		}

		$em = $this->getDoctrine()->getManager();

		$oEntityRepository = null;
		$oEntityRepository = $em->getRepository($sObjectType);
		$aResponse = array();

		if($oEntityRepository === null) {
			return new Response("Incorrect parameters", 500);
		} else {
			$oEntity = $oEntityRepository->find($iId);
			if($oEntity !== NULL) {
				$em->remove($oEntity);
				$em->flush();

				$oResponse = new Response(json_encode(true));
				$oResponse->setExpires(new \DateTime());
				return $oResponse;
			}
		}

		$oResponse = new Response(json_encode(false));
		$oResponse->setExpires(new \DateTime());
		return $oResponse;
	}

	/**
	 * Remove image
	 *
	 * @Route("/image/remove", name="dsj_cms_api_remove_image")
	 * @Template()
	 */
	public function removeImageAction()
	{
		$oRequest = $this->getRequest();
		$sObject = $oRequest->get('object', null);
		$iId = $oRequest->get('id', null);
		$sUploadableField = $oRequest->get('field', null);
		$sMapping = $oRequest->get('mapping', null);
		$sField = $oRequest->get('name_field', null);

		if($sObject === null || $iId === null || $sUploadableField === null || $sMapping === null) {
			return new Response("Incorrect request, missing parameters", 500);
		}

		$oEm = $this->getDoctrine()->getManager();
		$oEntity = $oEm->getRepository($sObject)->find($iId);

		if($oEntity !== NULL) {
            $sFile = $this->get('vich_uploader.storage')->resolvePath($oEntity, $sUploadableField);

            if(file_exists($sFile))
            {
                $bResult = unlink($sFile);
            }
            else
            {
                $bResult = true;
            }

			if($bResult === true) {
				call_user_func(array($oEntity, 'set' . $sField), null);
				$oEm->persist($oEntity);
				$oEm->flush();

				return new Response(json_encode(array('result' => true)));
			} else {
				return new Response(json_encode(array('result' => false)));
			}
		}

		return new Response("Couldn't find object", 500);
	}

	/**
	 * @Route("/page-sequence/update", name="dsj_cms_api_update_page_sequence")
	 */
	public function updatePageSequenceAction()
	{
		$oRequest = $this->getRequest();
		$oEm = $this->getDoctrine()->getManager();

		$iNewParentId = $oRequest->get('newParentId', null);
		$sEntity = urldecode($oRequest->get('entity', null));
		$aChildren = $oRequest->get('children', null);
		$iNewParentId = ($iNewParentId === '' ? NULL : $iNewParentId);
		$this->handleSequencing($aChildren, $iNewParentId, $sEntity);

		$oResponse = new Response(json_encode(true));
		$oResponse->setExpires(new \DateTime());

		return $oResponse;
	}

	private function handleSequencing($aChildren, $iNewParentId, $sEntity)
	{
		$oEm = $this->getDoctrine()->getManager();
		if(sizeof($aChildren)>1) {
			$oNewParent=NULL;
			$i=0;
			foreach($aChildren as $iChild) {
				$oItem = $oEm->getRepository("DSJCMSDBBundle:".$sEntity)->find($iChild);

				if ($oItem != null) {
					if ($iNewParentId !== NULL && ($oItem->getParent() === NULL || $oItem->getParent()->getId() !== $iNewParentId)) {
						if ($oNewParent === null) {
							$oNewParent = $oEm->getRepository("DSJCMSDBBundle:".$sEntity)->find(
								$iNewParentId
							);
						}
						$oItem->setParent($oNewParent);
					} else {
						$oItem->setParent(NULL);
					}
					$oItem->setSequence($i);
					$i++;
				}
			}
		} elseif(sizeof($aChildren) > 0) {
			$oItem = $oEm->getRepository("DSJCMSDBBundle:".$sEntity)->find($aChildren[0]);
			$oItem->setSequence(0);
			if($iNewParentId !== NULL) {
				if ($oItem->getParent() === null || $oItem->getParent()->getId() !== $iNewParentId) {
					$oNewParent = $oEm->getRepository("DSJCMSDBBundle:" . $sEntity)->find($iNewParentId);
					$oItem->setParent($oNewParent);
				}
			}
			$oEm->flush();
		}
		$oEm->flush();
	}

	/**
	 * @Route("/color/hex", name="dsj_cms_api_get_color_hex")
	 */
	public function getColorHexAction()
	{
		$oRequest = $this->getRequest();
		$iColor = $oRequest->get('color', null);

		if($iColor !== NULL) {
			$oEm = $this->getDoctrine()->getManager();
			/** @var Color $oColor */
			$oColor = $oEm->getRepository('DSJCMSDBBundle:System\Color')->find($iColor);
			if($oColor !== NULL) {
				return new Response(json_encode(array('hex' => $oColor->getColorCode())));
			}
		}
		return new Response(json_encode(array('hex' => '#FFFFFF')));
	}

	/**
	 * @Route("/content-type", name="dsj_cms_api_get_content_type")
	 */
	public function getContentTypeAction()
	{
		$oRequest = $this->getRequest();
		$iId = $oRequest->get('id', null);

		if($iId !== NULL) {
			$oEm = $this->getDoctrine()->getManager();
			/** @var ContentType $oContentType */
			$oContentType = $oEm->getRepository('DSJCMSDBBundle:Content\ContentType')->find($iId);
			if($oContentType !== NULL) {
				return new Response(json_encode(array('fields' => explode(',', $oContentType->getFields()))));
			}
		}

		return new Response(json_encode(array('fields' => NULL)));
	}

    /**
     * @Route("/multiple-image-upload", name="admin_api_multiple_image_upload")
     * @Template()
     */
    public function multipleImageUploadAction()
    {
        $oRequest = $this->getRequest();
        $oEm    = $this->getDoctrine()->getManager();

        $aFile = $oRequest->files->get('file');
        $sClass = $oRequest->query->get('object');
        $sRelation = $oRequest->query->get('amp;relation');

        $oEntity = new $sClass;

        $sRepository = $oRequest->query->get('amp;repository');
        $sParentRepository = $oRequest->query->get('amp;parent_repository');

        $oParentEntity = $oEm->getRepository($sParentRepository)->find($oRequest->query->get('amp;parent_object_id'));
        $aImages = $oParentEntity->getImages();

        if(sizeof($aImages)>0) {
            $iHighestSequence=0;
            foreach($aImages as $oImage) {
                if($oImage->getSequence() >= $iHighestSequence) {
                    $iHighestSequence = $oImage->getSequence() + 1;
                }
            }
            $oEntity->setSequence($iHighestSequence);
        } else {
            $oEntity->setSequence(0);
        }

        $oEntity->setParent($oParentEntity, $sRelation);
        $oEntity->setImageFile($aFile);
        $oEm->persist($oEntity);
        $oEm->flush();

        $sImageProp = $oRequest->query->get('amp;image_prop');
        $sFilterName = $oRequest->query->get('amp;filter_name');

        return $this->render('DSJCMSBackofficeBundle:Partials:image.html.twig', array(
            'object' => $oEntity,
            'imageProp' => $sImageProp,
            'filterName' => $sFilterName,
            'class' => $sRepository
        ));
    }

    /**
     *
     * @Route("/delete-image", name="admin_api_delete_image")
     * @Method("POST")
     * @Template()
     */
    public function deleteImageAction()
    {
        $oRequest = $this->getRequest();
        $oEm    = $this->getDoctrine()->getManager();

        $sObject = $oRequest->get("object");
        $iId = $oRequest->get("id");

        $oTargetImage = $oEm->getRepository($sObject)->find($iId);

        $aImages = $oEm->getRepository($sObject)->findGreaterThanSequence($oTargetImage->getSequence(), $oTargetImage->getParent()->getId());

        $oEm->remove($oTargetImage);
        foreach($aImages as $oImage) {
            $oImage->setSequence($oImage->getSequence()-1);
        }
        $oEm->flush();

        return new Response(json_encode(array('result' => 'ok')));
    }

    /**
     *
     * @Route("/update-image-position", name="admin_api_update_image_position")
     * @Method("POST")
     * @Template()
     */
    public function updateImagePositionAction()
    {
        $oRequest = $this->getRequest();
        $oEm    = $this->getDoctrine()->getManager();

        $aImagesNewPosition = json_decode($oRequest->get("images"));

        $i=0;
        foreach($aImagesNewPosition as $aImage) {
            $oTargetImage = $oEm->getRepository($aImage->object)->find($aImage->id);
            if($oTargetImage->getSequence() != $i) {
                $oTargetImage->setSequence($i);
            }
            $i++;
        }
        $oEm->flush();

        return new Response(json_encode(array('result' => 'ok')));
    }

    /**
     * @Route("/image/update-title", name="admin_api_update_image_title")
     */
    public function updateImageTitleValue()
    {
        $oRequest = $this->getRequest();

        /** @var SubdomainManager $oSiteManager **/
        $oSiteManager = $this->container->get('subdomain_manager');
        $oEm = $oSiteManager->getManager();

        $sField = $oRequest->get('name', null);
        $sValue = $oRequest->get('value', null);
        $iId = $oRequest->get('pk', null);
        $sRepository = $oRequest->get('repository', null);

        if($iId === null || $sField === NULL || $sValue === NULL) {
            return new Response("Incorrect request, missing parameters", 500);
        }

        $oImage = $oEm->getRepository($sRepository)->find($iId);

        call_user_func(array($oImage, 'set'.ucfirst($sField)), $sValue);
        $oEm->flush();

        $oResponse = new Response(json_encode(true));
        $oResponse->setExpires(new \DateTime());
        return $oResponse;
    }
}
