<?php

namespace DSJ\CMS\BackofficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

use Doctrine\ORM\EntityManager;

/**
 * Backend System controller.
 *
 */
class SystemController extends Controller
{
    /**
     * @Route("/", name="dsj_cms_index")
     * @Template()
     */
    public function indexAction()
    {

        return array(
            'menu'        => 'index',
            'submenu'     => 'index',
        );
    }

    /**
     * @Route("/login", name="dsj_cms_login")
     * @Template()
     */
    public function loginAction()
    {
        $request = $this->container->get('request');
        /* @var $request \Symfony\Component\HttpFoundation\Request */
        $session = $request->getSession();
        /* @var $session \Symfony\Component\HttpFoundation\Session */

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        if ($error) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        }
        // last username entered by the user
        $lastUsername = $request->cookies->get('dsjcmsbackoffice_lastUsername','');

        return array(
            'last_username' => $lastUsername,
            'error'         => $error,
        );
    }

    /**
     * @Route("/login_check", name="dsj_cms_login_check")
     * @Template()
     */
    public function loginCheckAction() {

    }

    /**
     * @Route("/logout", name="dsj_cms_logout")
     * @Template()
     */
    public function logoutAction() {

    }
    
    /**
     * @Route("/wachtwoord-vergeten", name="dsj_cms_forgot_password")
     * @Method("POST")
     */
    public function forgotPasswordAction()
    {
    	$oEm = $this->getDoctrine()->getManager();
    	$oRequest = $this->getRequest();
    	$sEmailAddress = $oRequest->request->get('email');
    	
    	$oUser = $oEm->getRepository('DSJCMSDBBundle:System\User')->findOneByEmailAddress($sEmailAddress);
    	if($oUser === null) {
    		throw new \Exception("User not available");
    	}
    	
    	$oEmail = $oEm->getRepository('DSJCMSDBBundle:System\Email')->findOneBySlug('wachtwoord-vergeten');
        if($oEmail === null) {
            throw new \Exception("Email not available");
        }

        $sSubject = $oEmail->getSubject();
        $sContent = $oEmail->getContent();
        
        $sUrl = $this->generateUrl('dsj_cms_account_set_password', array('id' => $oUser->getId(), 'token' => sha1($oUser->getEmailAddress().'-'.$oUser->getCreated()->format('Y-m-d').'-'.$oUser->getId())), true);
        $sContent = str_replace("%url%", $sUrl, $sContent);
		$sContent = str_replace("%link%", $sUrl, $sContent);
        $sContent = str_replace("%name%", $oUser->getName(), $sContent);
		$sContent = str_replace("%firstname%", $oUser->getName(), $sContent);

        $aTo = array();
        $aTo[] = $oUser->getEmailAddress();

        $sEmail = $this->render('Store24FrontendBundle:Email:layout.html.twig', array(
        		'httpHost' => $this->container->getParameter('http_host'),
        		'content' => $sContent,
        		'subject' => $sSubject,
        ))->getContent();
        $message = \Swift_Message::newInstance()
            ->setSubject($sSubject)
            ->setFrom($this->container->getParameter('mail_from'))
            ->setTo($aTo)
            ->setBody($sEmail, 'text/html')
        ;
        
        $this->get('mailer')->send($message);
        return new Response("<p>Er is een e-mail naar u gestuurd, volg de aanwijzingen in die e-mail om uw wachtwoord te wijzigen</p>");
    }
    
    /**
     * @Route("/wachtwoord-instellen", name="dsj_cms_account_set_password")
     * @Template()
     */
    public function setPasswordAction() {
    
    	$oEm = $this->getDoctrine()->getManager();
    	$oRequest = $this->getRequest();
    
    	$iUser = $oRequest->get('id',null);
    	$sToken = $oRequest->get('token',null);
    	
    	if($iUser !== null)
    	{
    		$oUser = $oEm->getRepository('DSJCMSDBBundle:System\User')->find($iUser);
    		if($sToken != sha1($oUser->getEmailAddress().'-'.$oUser->getCreated()->format('Y-m-d').'-'.$oUser->getId())) {
    			throw $this->createNotFoundException();
    		}
    	}
    
    	$oForm = $this->createForm(new PasswordForm());
    	if($oRequest->getMethod() == 'POST') {
    		$oForm->bind($oRequest);
    		if($oForm->isValid()) {
    			$aPassword = $oForm->getData('password');
    			$sPassword = $aPassword['password'];
    			
                if(strlen(trim($sPassword))>0) {
                    /** @var AdminEncoder $oEncoder  */
                    $oEncoder = $this->get('store24.security.adminencoder');

                    $oUser->setPassword($oEncoder->encodePassword($sPassword,$oUser->getSalt()));
                    $oEm->flush();
                }
    		}
    	}
    	 
    	return array(
    			'iUser'     => $iUser,
    			'sToken' 	=> $sToken,
    			'form'      => $oForm->createView(),
    			'valid'     => $oForm->isBound() && $oForm->isValid(),
    	);
    }
    
    /**
     * @Route("/menu", name="dsj_cms_menu")
     * @Template()
     */
    public function menuAction($menu=null, $submenu=null) {
        $aBackofficeMenu = array();
        if($this->container->hasParameter('dsj.cms.backoffice')) {
            $aBackofficeMenu = $this->container->getParameter('dsj.cms.backoffice');
            $aBackofficeMenu = $aBackofficeMenu['menu'];
        }

        if($this->get('templating')->exists('DSJCMSFrontendBackofficeBundle:System:menu.html.twig')) {
            return $this->render('DSJCMSFrontendBackofficeBundle:System:menu.html.twig', array(
                    'menu'	=> $menu,
                    'submenu'	=> $submenu,
                    'aMenu' => $aBackofficeMenu,
                ));
        }
    	
    	return array(
            'menu'	=> $menu,
            'submenu'	=> $submenu,
            'aMenu' => $aBackofficeMenu,
    	);
    }

}
