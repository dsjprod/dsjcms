<?php
namespace DSJ\CMS\BackofficeBundle\Controller\Account;

use Doctrine\ORM\EntityManager;
use DSJ\CMS\BackofficeBundle\Form\UserType;
use DSJ\CMS\DBBundle\Entity\Account\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class UserController
 *
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/", name="dsj_cms_user")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DSJCMSDBBundle:Account\User')->findAll();

        return array(
            'menu' => 'settings',
            'submenu'     => 'user',
            'entities' => $entities
        );
    }


    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="dsj_cms_user_new")
     * @Template()
     */
    public function newAction()
    {
        $oRequest = $this->getRequest();
        $oEntity   = new User();
        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('user', $aForms) ? $aForms['user'] : array());
        $form     = $this->createForm(new UserType(), $oEntity, array('fields' => $aFields));
        $oEm      = $this->getDoctrine()->getManager();

        if($oRequest->isMethod("POST")) {
            $form->handleRequest($oRequest);

            if ($form->isValid()) {
                $oEntity->setSalt(sha1(get_class($oEntity).time()));

                $sPlainPassword = $form->get('plainPassword')->getData();
                if(strlen(trim($sPlainPassword))>0) {
                    /** @var AdminEncoder $oEncoder  */
                    $oEncoder = $this->get('dsj.cms.security.adminencoder');

                    $oEntity->setPassword($oEncoder->encodePassword($sPlainPassword,$oEntity->getSalt()));
                }

                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_user_edit', array('id' => $oEntity->getId())));
            }
        }

        return array(
            'menu'   => 'settings',
            'submenu'     => 'user',
            'entity' => $oEntity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="dsj_cms_user_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $oRequest = $this->getRequest();
        $oEm      = $this->getDoctrine()->getManager();
        /** @var User $oEntity */
        $oEntity   = $oEm->getRepository('DSJCMSDBBundle:Account\User')->find($id);
        $aForms = $this->container->getParameter('dsj.cms.form');
        $aFields = (array_key_exists('user', $aForms) ? $aForms['user'] : array());
        $form     = $this->createForm(new UserType(), $oEntity, array('fields' => $aFields));

        if (!$oEntity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        if($oRequest->isMethod("POST")) {
            $form->handleRequest($oRequest);

            if ($form->isValid()) {

                $oEntity->setUpdated(new \DateTime());
                $oEm->persist($oEntity);

                $sPlainPassword = $form->get('plainPassword')->getData();
                if(strlen(trim($sPlainPassword))>0) {
                    /** @var AdminEncoder $oEncoder  */
                    $oEncoder = $this->get('dsj.cms.security.adminencoder');

                    $oEntity->setPassword($oEncoder->encodePassword($sPlainPassword,$oEntity->getSalt()));
                }

                $oEm->persist($oEntity);
                $oEm->flush();

                return $this->redirect($this->generateUrl('dsj_cms_user_edit', array('id' => $oEntity->getId())));

            }
        }

        return array(
            'menu'        => 'settings',
            'submenu'     => 'user',
            'entity'      => $oEntity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}/delete", name="dsj_cms_user_delete")
     */
    public function deleteAction($id)
    {
        $oEm      = $this->getDoctrine()->getManager();

        $oUser = $oEm->getRepository('DSJCMSDBBundle:Account\User')->find($id);

        if (!$oUser) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $oEm->remove($oUser);
        $oEm->flush();

        return $this->redirect($this->generateUrl('dsj_cms_user'));
    }
    
    /**
     * @Template()
     */
    public function showThumbnailAction($id)
    {
        $oEm = $this->getDoctrine()->getManager();

        $oUser = $oEm->getRepository('DSJCMSDBBundle:Account\User')->find($id);

        if (!$oUser) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        return array(
            'user' => $oUser
        );
    }
}