<?php

namespace DSJ\CMS\BackofficeBundle\Command;

use DSJ\CMS\DBBundle\Entity\Account\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

class CreateAdminUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('admin:user:create')
            ->setDescription('Creates an admin User')
            ->addArgument('name', InputArgument::REQUIRED, 'Please give the user\'s name?')
            ->addArgument('email', InputArgument::REQUIRED, 'Please give the email address?')
            ->addArgument('password', InputArgument::REQUIRED, 'Please give the password?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sName = $input->getArgument('name');
        $sEmail = $input->getArgument('email');
        $sPassword = $input->getArgument('password');

        /** @var EntityManager $oEm  */
        $oEm = $this->getContainer()->get('doctrine')->getManager();

        $factory = $this->getContainer()->get('security.encoder_factory');
        $oUser = new User();
        $oUser->setName($sName);
        $oUser->setEmail($sEmail);
        $oUser->setSalt(substr(md5(time()),0,16));
        $encoder = $factory->getEncoder($oUser);
        $sPassword = $encoder->encodePassword($sPassword, $oUser->getSalt());
        $oUser->setPassword($sPassword);
        $oEm->persist($oUser);
        $oEm->flush();
    }
}
