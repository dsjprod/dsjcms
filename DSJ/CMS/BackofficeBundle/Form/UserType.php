<?php

namespace DSJ\CMS\BackofficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'fields',
        ));

        $resolver->setAllowedTypes(array(
            'fields' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\DBBundle\Entity\Account\User'
        ));
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $aFields = $options['fields'];
        if(in_array('name', $aFields) || sizeof($aFields) === 0)
            $builder->add('name', null, array('label' => 'Naam'));
        if(in_array('email', $aFields) || sizeof($aFields) === 0)
            $builder->add('email', null, array('label' => 'E-mailadres'));
        if(in_array('imageFile', $aFields) || sizeof($aFields) === 0)
            $builder->add('imageFile', 'image_upload', array(
                'label' => 'Afbeelding:',
                'previewfilter' => 'image_preview',
                'required' => false,
                'mapping'   => 'user_image',
                'file_name_field'   => 'image',
            ));
        if(in_array('password', $aFields) || sizeof($aFields) === 0)
            $builder->add('plainPassword', 'repeated', array('required' => false, 'type' => 'password', 'mapped' => null, 'first_options' => array('label' => 'Wachtwoord'), 'second_options' => array('label' => 'Wachtwoord Herhalen')));
    }

    public function getName()
    {
        return 'dsj_cms_admin_user';
    }
}
