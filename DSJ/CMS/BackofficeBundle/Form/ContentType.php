<?php

namespace DSJ\CMS\BackofficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContentType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'fields',
        ));

        $resolver->setAllowedTypes(array(
            'fields' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\FrontendDBBundle\Entity\Content\Content'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if((in_array('uniqueTitle', $options['fields']) || sizeof($options['fields']) === 0) && $builder->getData()->getUniqueTitle() == NULL)
            $builder->add("uniqueTitle", null, array("label" => "Unieke naam (eenmalig in te stellen):", "required" => false));
        if(in_array('title', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("title", null, array("label" => "Titel:", "required" => false));
        if(in_array('content', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("content", "textarea", array("label" => "Inhoud:", "required" => false, 'attr' => array('class' => 'ckeditor')));
        if(in_array('link', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("link", null, array("label" => "Link", "required" => false));
        if(in_array('linkText', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("linkText", null, array("label" => "Link tekst:", "required" => false));
        if(in_array('imageFile', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('imageFile', 'image_upload', array(
                'label' => 'Afbeelding:',
                'previewfilter' => 'image_preview',
                'required' => false,
                'mapping'   => 'slide_image',
                'file_name_field'   => 'image',
            ));
        ;
    }

    public function getName()
    {
        return 'dsj_cms_content';
    }
}
