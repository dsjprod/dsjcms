<?php

namespace DSJ\CMS\BackofficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SlideType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'fields',
        ));

        $resolver->setAllowedTypes(array(
            'fields' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\FrontendDBBundle\Entity\Content\Slide'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(in_array('title', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("title", null, array("label" => "Titel", "required" => false));
        if(in_array('backendTitle', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("backendTitle", null, array("label" => "Interne titel", "required" => false));
        if(in_array('subtitle', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("subtitle", null, array("label" => "Subtitel", "required" => false));
        if(in_array('link', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("link", null, array("label" => "Link", "required" => false));
        if(in_array('buttonText', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("buttonText", null, array("label" => "Tekst op knop", "required" => false));
        if(in_array('imageFile', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('imageFile', 'image_upload', array(
                'label' => 'Afbeelding:',
                'previewfilter' => 'image_preview',
                'required' => false,
                'mapping'   => 'slide_image',
                'file_name_field'   => 'image',
            ));
        if(in_array('active', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('active', 'checkbox', array('label' => 'Actief', 'required' => false));
        if(in_array('sequence', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('sequence', 'integer', array('label' => 'Volgorde', 'required' => false));
        ;
    }

    public function getName()
    {
        return 'dsj_cms_slide';
    }
}
