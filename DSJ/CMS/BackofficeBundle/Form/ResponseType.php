<?php

namespace DSJ\CMS\BackofficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResponseType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'fields',
        ));

        $resolver->setAllowedTypes(array(
            'fields' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\FrontendDBBundle\Entity\System\Response'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(in_array('title', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("title", null, array("label" => "Titel:", "required" => false));
        if(in_array('name', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("name", null, array("label" => "Naam:", "required" => false));
        if(in_array('location', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("location", null, array("label" => "Locatie:", "required" => false));
        if(in_array('active', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('active', 'checkbox', array('label' => 'Actief:', 'required'  => false,));
        if(in_array('imageFile', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('imageFile', 'image_upload', array(
                'label' => 'Afbeelding:',
                'previewfilter' => 'image_preview',
                'required' => false,
                'mapping'   => 'response_image',
                'file_name_field'   => 'image',
            ));
        if(in_array('imageFileLogo', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('imageFileLogo', 'image_upload', array(
                'label' => 'Logo:',
                'previewfilter' => 'image_preview',
                'required' => false,
                'mapping'   => 'response_logo_image',
                'file_name_field'   => 'imageLogo',
            ));
        if(in_array('quote', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('quote', 'text', array('label' => 'Citaat (slider):', 'required'  => false));
        if(in_array('description', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('description', 'textarea', array('label' => 'Beschrijving:', 'required'  => false));
        if(in_array('content', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('content', 'textarea', array('label' => 'Inhoud:', 'required'  => false, 'attr'  => array('class' => 'ckeditor')));
    }

    public function getName()
    {
        return 'dsj_cms_response';
    }
}
