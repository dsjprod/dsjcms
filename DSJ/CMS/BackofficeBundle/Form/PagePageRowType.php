<?php
namespace DSJ\CMS\BackofficeBundle\Form;

use Doctrine\ORM\EntityRepository;
use DSJ\CMS\BackofficeBundle\Form\DataTransformer\PageToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PagePageRowType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(
            array(
                'em',
                'forms'
            )
        );

        $resolver->setAllowedTypes(
            array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
                'forms' => 'array',
            )
        );

        $resolver->setDefaults(
            array(
                'data_class' => 'DSJ\CMS\DBBundle\Entity\Content\Page'
            )
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];

        $builder->add(
            'pageRows',
            'collection',
            array(
                'label' => 'Regels:',
                'type' => new PageRowType(),
                'options' => array(
                    'em' => $entityManager,
                    'forms' => $options['forms']
                ),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
            )
        )
        ->add('addRow', 'hidden', array('mapped' => false, 'required' => false))
        ->add('newRow', 'submit', array('label' => '<i class="fa fa-plus"></i> Nieuwe regel'))
    ;
    }

    public function getName()
    {
        return 'dsj_cms_pagepagerow';
    }
}