<?php
namespace DSJ\CMS\BackofficeBundle\Form\DataTransformer;

use DSJ\CMS\BackofficeBundle\Form\PageContentType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class ContentTypeToIdTransformer implements DataTransformerInterface {
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (product) to a string (title).
     *
     * @param  PageContentType|null $contentType
     * @return string
     */
    public function transform($contentType)
    {
        if (null === $contentType) {
            return "";
        }

        $output = $contentType->getId();
        return $output;
    }

    /**
     * Transforms a string (id) to an object (product).
     *
     * @param  string $input
     *
     * @return PageContentType|null
     *
     * @throws TransformationFailedException if object (product) is not found.
     */
    public function reverseTransform($input)
    {
        if (!$input) {
            return null;
        }

        $contentType = $this->om
            ->getRepository('DSJCMSDBBundle:Content\ContentType')
            ->findOneBy(array('id' => $input))
        ;

        if (null === $contentType) {
            throw new TransformationFailedException(sprintf(
                'A type with id "%s" does not exist!',
                $input
            ));
        }

        return $contentType;
    }
} 