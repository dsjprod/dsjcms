<?php
namespace DSJ\CMS\BackofficeBundle\Form\DataTransformer;

use DSJ\CMS\DBBundle\Entity\Content\Page;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

class PageToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (page) to a string (title).
     *
     * @param  Page|null $page
     * @return string
     */
    public function transform($page)
    {
        if (null === $page) {
            return "";
        }

        $output = $page->getTitle() . ' (id: ' . $page->getId().')';

        return $output;
    }

    /**
     * Transforms a string (id) to an object (page).
     *
     * @param  string $input
     *
     * @return Page|null
     *
     * @throws TransformationFailedException if object (page) is not found.
     */
    public function reverseTransform($input)
    {
        if (!$input || stristr($input, '(id:') === false) {
            return null;
        }

        list($sTitle, $id) = explode(" (id: ", $input);
        $id = str_replace(")","",$id);

        $page = $this->om
            ->getRepository('DSJCMSDBBundle:Content\Page')
            ->find($id)
        ;

        if (null === $page) {
            throw new TransformationFailedException(sprintf(
                'A page with title "%s" does not exist!',
                $id
            ));
        }

        return $page;
    }
} 