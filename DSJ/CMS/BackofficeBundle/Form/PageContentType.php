<?php
namespace DSJ\CMS\BackofficeBundle\Form;

use Doctrine\ORM\EntityRepository;
use DSJ\CMS\BackofficeBundle\Form\DataTransformer\ContentTypeToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PageContentType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'em', 'forms'
        ));

        $resolver->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
            'forms' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\DBBundle\Entity\Content\Content'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $aFields = (array_key_exists('content', $options['forms']) ? $options['forms']['content'] : array());
        $entityManager = $options['em'];

        if(in_array('title', $aFields) || sizeof($aFields) === 0)
            $builder->add('title', null, array('label' => 'Titel:', 'attr'  => array('class' => 'contentTitle'), 'required' => false));
        if(in_array('backendTitle', $aFields) || sizeof($aFields) === 0)
            $builder->add('backendTitle', null, array('label' => 'Interne titel:', 'attr'  => array('class' => 'contentBackendTitle'), 'required' => false));
        if(in_array('content', $aFields) || sizeof($aFields) === 0)
            $builder->add('content', 'textarea', array('label' => 'Inhoud:', 'attr'  => array('class' => 'ckeditor contentContent'), 'required' => false));
        if(in_array('imageFile', $aFields) || sizeof($aFields) === 0)
            $builder->add('imageFile', 'image_upload', array(
                'label' => 'Afbeelding:',
                'label_attr' => array('class' => 'contentImage'),
                'attr'  => array('class' => 'contentImage'),
                'previewfilter' => 'image_preview',
                'required' => false,
                'mapping'   => 'content_image',
                'file_name_field'   => 'image',
            ));
        if(in_array('imageAltText', $aFields) || sizeof($aFields) === 0)
            $builder->add('imageAltText', null, array('label' => 'Alternatieve tekst afbeelding (SEO):', 'attr'  => array('class' => 'imageAltText'), 'required' => false));
        if(in_array('imageAlign', $aFields) || sizeof($aFields) === 0)
            $builder->add('imageAlign', 'choice', array(
                'label'   => 'Afbeelding uitlijnen:',
                'choices' => array(
                    'top' => 'Boven',
                    'left' => 'Links',
                    'right' => 'Rechts',
                ),
                'attr'    => array(
                    'class' => 'contentImageAlign'
                )));
        if(in_array('form', $aFields) || sizeof($aFields) === 0)
            $builder->add('form', 'entity', array(
                'label'     => 'Formulier:',
                'class'     => 'DSJCMSDBBundle:Content\Form',
                'property'  => 'title',
                'required'  => false,
                'attr'  => array('class' => 'form')
            ));
        if(in_array('active', $aFields) || sizeof($aFields) === 0)
            $builder->add('active', 'checkbox', array('label' => 'Actief:', 'required'  => false, 'attr' => array('class' => 'contentActive')));

        $builder->add('sequence', 'hidden', array('attr' => array('class' => 'sequence'),));
        $builder->add('size', 'hidden', array('attr' => array('class' => 'type'),));

        if(in_array('type', $aFields) || sizeof($aFields) === 0) {
            $transformer = new ContentTypeToIdTransformer($entityManager);
            $builder->add($builder->create('type', 'hidden', array(
                    'required' => false,
                    'attr' => array('class' => 'contentTypeId'),
                ))->addModelTransformer($transformer)
            );
        }
    }

    public function getName()
    {
        return 'dsj_cms_content';
    }
} 