<?php

namespace DSJ\CMS\BackofficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageTagType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tag', 'entity',
                array(
                'label'     => 'tag',
                'required'  => false,
                'class'     => 'DSJCMSDBBundle:System\Tag',
                'multiple'  => false,
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\DBBundle\Entity\Content\PageTag'
        ));
    }

    public function getName()
    {
        return 'dsj_cms_page_tag';
    }
}
