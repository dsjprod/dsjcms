<?php

namespace DSJ\CMS\BackofficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmailType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'fields',
        ));

        $resolver->setAllowedTypes(array(
            'fields' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\DBBundle\Entity\System\Email'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if((in_array('identifier', $options['fields']) || sizeof($options['fields']) === 0) && $builder->getData()->getIdentifier() == NULL)
            $builder->add("identifier", null, array("label" => "Unieke naam (eenmalig in te stellen):", "required" => false));
        if(in_array('title', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("title", null, array("label" => "Interne titel:", "required" => false));
        if(in_array('subject', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("subject", null, array("label" => "Onderwerp:", "required" => false));
        if(in_array('contentTitle', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("contentTitle", null, array("label" => "Titel:", "required" => false));
        if(in_array('content', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("content", 'textarea', array("label" => "Inhoud:", "required" => false, 'attr'  => array('class' => 'ckeditor')));
    }

    public function getName()
    {
        return 'dsj_cms_email';
    }
}
