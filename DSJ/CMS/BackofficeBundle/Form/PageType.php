<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Geo
 * Date: 16-1-14
 * Time: 18:49
 * To change this template use File | Settings | File Templates.
 */

namespace DSJ\CMS\BackofficeBundle\Form;

use Doctrine\ORM\EntityRepository;
use DSJ\CMS\BackofficeBundle\Form\DataTransformer\PageToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PageType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'em', 'forms'
        ));

        $resolver->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
            'forms' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\DBBundle\Entity\Content\Page'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $aFields = (array_key_exists('page', $options['forms']) ? $options['forms']['page'] : array());

        if((in_array('page', $aFields) || sizeof($aFields) === 0) && $builder->getData()->getPage() == NULL)
            $builder->add('page', null, array('label' => 'Unieke naam (eenmalig in te stellen):'));
        if(in_array('title', $aFields) || sizeof($aFields) === 0)
            $builder->add('title', null, array('label' => 'Titel op de pagina:'));
        if(in_array('backendTitle', $aFields) || sizeof($aFields) === 0)
            $builder->add('backendTitle', null, array('label' => 'Interne titel:'));

        $builder->add('pageTitle', null, array('label' => 'Titel in de URL:', 'required' => true)); // required as slug depends on this field
        if(in_array('menuTitle', $aFields) || sizeof($aFields) === 0)
            $builder->add('menuTitle', null, array('label' => 'Titel in het menu (mits toepassing):'));
        if(in_array('parent', $aFields) || sizeof($aFields) === 0) {
            $transformerPageToId = new PageToIdTransformer($entityManager);
            $builder->add($builder->create('parent', 'text',
                array(
                    'label' => 'Bovenliggende pagina: ',
                    'required' => false,
                    'attr' => array('class' => 'typeahead', 'data-class' => 'page', 'placeholder' => 'Begin met typen...')
                ))->addModelTransformer($transformerPageToId)
            );
        }

        if(in_array('seoTitle', $aFields) || sizeof($aFields) === 0)
            $builder->add('seoTitle', null, array('label' => 'SEO titel:'));
        if(in_array('seoDescription', $aFields) || sizeof($aFields) === 0)
            $builder->add('seoDescription', 'textarea', array('label' => 'SEO Omschrijving:', 'required'  => false,));

        if(in_array('active', $aFields) || sizeof($aFields) === 0)
            $builder->add('active', 'checkbox', array('label'     => 'Actief:', 'required'  => false,));
        if(in_array('menuItem', $aFields) || sizeof($aFields) === 0)
            $builder->add('menuItem', 'checkbox', array('label'     => 'Menu item:', 'required'  => false,));
        if(in_array('breadcrumbItem', $aFields) || sizeof($aFields) === 0)
            $builder->add('breadcrumbItem', 'checkbox', array('label'     => 'Toon in kruimelpad:', 'required'  => false,));
        if(in_array('introduction', $aFields) || sizeof($aFields) === 0)
            $builder->add('introduction', 'textarea', array('label' => 'Introductie:', 'required'  => false,));
        if(in_array('content', $aFields) || sizeof($aFields) === 0)
            $builder->add('content', 'textarea', array('label' => 'Inhoud:', 'required'  => false, 'attr'  => array('class' => 'ckeditor')));
        if(in_array('imageFile', $aFields) || sizeof($aFields) === 0)
            $builder->add('imageFile', 'image_upload', array(
                'label' => 'Afbeelding:',
                'previewfilter' => 'image_preview',
                'required' => false,
                'mapping'   => 'page_image',
                'file_name_field'   => 'image',
            ));

        if(in_array('imageAlign', $aFields) || sizeof($aFields) === 0)
            $builder->add('imageAlign', 'choice', array(
                'label'   => 'Afbeelding uitlijnen:',
                'choices' => array(
                    'none' => '(geen)',
                    'topLeft' => 'Linksboven',
                    'top' => 'Boven',
                    'topRight' => 'Rechtsboven',
                    'left' => 'Links',
                    'center' => 'Midden',
                    'right' => 'Rechts',
                    'bottomLeft' => 'Linksonder',
                    'bottom' => 'Onder',
                    'bottomRight' => 'Rechtsonder'
                ),
                'attr'    => array(
                    'class' => 'contentImageAlign'
                )));

        if(in_array('pageTags', $aFields) || sizeof($aFields) === 0)
            $builder->add('pageTags', 'autocomplete_collection', array(
                'label'         => 'Tags',
                'type'          => new PageTagType(),
                'class'         => 'DSJCMSDBBundle:System\Tag',
                'allow_add'     => true,
                'allow_delete'  => true,
                'by_reference'  => false,
                'required'      => false
            ));
        if(in_array('includeTagCloud', $aFields) || sizeof($aFields) === 0)
            $builder->add('includeTagCloud', 'checkbox', array('label' => 'Opnemen in tagcloud:', 'required'  => false,));
        if(in_array('tagCloudSize', $aFields) || sizeof($aFields) === 0)
            $builder->add("tagCloudSize", 'choice', array("label" => "Grootte in tagcloud:", "required" => false, 'choices' => array(1,2,3,4,5)));
        if(in_array('website', $aFields) || sizeof($aFields) === 0)
            $builder->add('website', 'entity', array('class' => 'DSJ\CMS\FrontendDBBundle\Entity\System\Website', 'label' => 'Website', 'required' => false));

    }

    public function getName()
    {
        return 'dsj_cms_page';
    }
}