<?php

namespace DSJ\CMS\BackofficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AlertType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'fields',
        ));

        $resolver->setAllowedTypes(array(
            'fields' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\DBBundle\Entity\System\Alert'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if((in_array('alert', $options['fields']) || sizeof($options['fields']) === 0))
            $builder->add("alert", null, array("label" => "Melding:", "required" => false));
        if(in_array('type', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("type", 'choice', array("label" => "Type melding:", "required" => false, 'choices' => \DSJ\CMS\DBBundle\Entity\System\AlertType::getSelectChoices()));
        if(in_array('description', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("description", 'textarea', array("label" => "Omschrijving (intern):", "required" => false));
    }

    public function getName()
    {
        return 'dsj_cms_alert';
    }
}
