<?php
/**
 * Created by PhpStorm.
 * User: george
 * Date: 2/4/14
 * Time: 9:22 AM
 */

namespace DSJ\CMS\BackofficeBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class PageRowType extends AbstractType {
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'em', 'forms'
        ));

        $resolver->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
            'forms'    => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\DBBundle\Entity\Content\PageRow'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $aFields = (array_key_exists('pageRow', $options['forms']) ? $options['forms']['pageRow'] : array());

        if(in_array('bgColor', $aFields))
            $builder->add('bgColor', 'color', array(
                'class'     => 'DSJCMSDBBundle:System\Color',
                'property'  => 'titleAndColorCodes',
                'label'     => 'Achtergrondkleur:',
                'required'  => false,
                'attr'      => array(
                    'class'     => 'bg-color',

                ),
            ));

        if(in_array('fullWidth', $aFields))
            $builder->add('fullWidth', 'checkbox', array('label' => 'Rij over gehele breedte?:', 'required'  => false));

        $builder
            ->add('sequence', 'hidden', array(
                'attr' => array('class' => 'sequence'),
            ))
            ->add('pageContent', 'collection', array(
                'label'        => 'Items:',
                'type'         => new PageContentType(),
                'options'      => array('em' => $options['em'], 'forms' => $options['forms']),
                'allow_add'    => true,
                'allow_delete' => true,
            	'by_reference' => false,
        ));
    }

    public function getName()
    {
        return 'edit_pagerow';
    }
} 