<?php

namespace DSJ\CMS\BackofficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TagType extends AbstractType
{
    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array(
            'fields',
        ));

        $resolver->setAllowedTypes(array(
            'fields' => 'array',
        ));

        $resolver->setDefaults(array(
            'data_class' => 'DSJ\CMS\DBBundle\Entity\System\Tag'
        ));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(in_array('tag', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add("tag", null, array("label" => "Tag", "required" => false));
        if(in_array('active', $options['fields']) || sizeof($options['fields']) === 0)
            $builder->add('active', 'checkbox', array('label' => 'Actief', 'required' => false));
    }

    public function getName()
    {
        return 'dsj_cms_tag';
    }
}
