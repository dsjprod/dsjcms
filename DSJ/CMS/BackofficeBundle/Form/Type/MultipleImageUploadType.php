<?php
namespace DSJ\CMS\BackofficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class MultipleImageUploadType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
        ));

        $resolver->setRequired(array('previewfilter'));
        $resolver->setRequired(array('object'));
        $resolver->setRequired(array('parent_repository'));
        $resolver->setRequired(array('parent'));

    }

    /**
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $image = null;

        $parentData = $form->getParent()->getData();
        if (null !== $parentData) {
            $accessor = PropertyAccess::createPropertyAccessor();
            $image = $accessor->getValue($parentData, $form->getName());
        }

        $view->vars['imageProp'] = $options['property'];
        $view->vars['imageAttribute'] = str_replace("File", "", $options['property']);
        $view->vars['object'] = $form->getParent()->getData();
        $view->vars['previewfilter'] = $options['previewfilter'];
        $view->vars['imageObject'] = $options['object'];
        $view->vars['parentRepo'] = $options['parent_repository'];
        $view->vars['class'] = $options['class'];
        $view->vars['relation'] = $options["parent"];
        $view->vars['hasImage'] = null!==$image;
    }

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'multiple_image_upload';
    }
}