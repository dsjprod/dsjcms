<?php
namespace DSJ\CMS\BackofficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Doctrine\ORM\EntityManager;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\Common\Collections\ArrayCollection;

class AutocompleteSingleType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array('multiple' => 'multiple', 'customMethod' => NULL));
        $resolver->setRequired(array('class'));
    }

    /**
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $oAccessor = PropertyAccess::createPropertyAccessor();
        $aChildren = $oAccessor->getValue($form->getParent()->getData(), $form->getName());

        $view->vars['autocompleteProp'] = $form->getName();
        $view->vars['children'] = $aChildren;
        $view->vars['object_id'] = $form->getParent()->getData()->getId();
        $view->vars['class'] = htmlspecialchars($options['class']);
        $view->vars['multiple'] = $options['multiple'];
        $view->vars['customMethod'] = $options['customMethod'];
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'autocomplete_single';
    }
}