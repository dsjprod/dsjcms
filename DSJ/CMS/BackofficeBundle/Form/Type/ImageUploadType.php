<?php
namespace DSJ\CMS\BackofficeBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ImageUploadType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
        ));

        $resolver->setRequired(array('previewfilter', 'mapping', 'file_name_field'));
    }

    /**
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $image = null;

        $parentData = $form->getParent()->getData();
        if (null !== $parentData) {
            $accessor = PropertyAccess::createPropertyAccessor();
            
            $image = $accessor->getValue($parentData, $options['file_name_field']);
        }
        $view->vars['imageProp'] = $form->getName();
        $view->vars['object'] = $form->getParent()->getData();
        $view->vars['previewfilter'] = $options['previewfilter'];
        $view->vars['mapping'] = $options['mapping'];
        $view->vars['file_name_field'] = $options['file_name_field'];
        $view->vars['hasImage'] = (null!==$image && ''!==$image);
    }

    public function getParent()
    {
        return 'file';
    }

    public function getName()
    {
        return 'image_upload';
    }
}