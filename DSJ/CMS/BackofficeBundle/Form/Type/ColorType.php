<?php
namespace DSJ\CMS\BackofficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ColorType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }

    /**
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
    }

    public function getParent()
    {
        return 'entity';
    }

    public function getName()
    {
        return 'color';
    }
}