function initPlupload() {
    var uploader = new plupload.Uploader({
        runtimes : 'html5,flash,silverlight,html4',

        browse_button : 'pickfiles', // you can pass in id...
        container: document.getElementById('container'), // ... or DOM Element itself

        url : decodeURI(urlApiUploadMultipleImages),

        filters : {
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png"}
            ]
        },

        sortable: true,

        views: {
            list: true,
            thumbs: true, // Show thumbs
            active: 'thumbs'
        },

        // Flash settings
        flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',

        // Silverlight settings
        silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',


        init: {
            PostInit: function() {
                document.getElementById('filelist').innerHTML = '';

                document.getElementById('uploadfiles').onclick = function() {
                    uploader.start();
                    return false;
                };
            },

            FilesAdded: function(up, files) {
                plupload.each(files, function(file) {
                    document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                });
            },

            UploadProgress: function(up, file) {
                document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
            },

            Error: function(up, err) {
                document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
            },

            FileUploaded: function (up, file, response) {
                $('#console').append(response.response);
                initEditableImages();
            }
        }
    });

    uploader.init();
    initEditableImages();
}

function init_sortable_images() {
    var oldPosition = null;
    var adjustment;
    $( "#console" ).sortable({
        itemSelector: 'div.image_holder',
        handle: 'div.image_holder',
        vertical: false,
        placeholder: '<div class="image_placeholder"></div>',
        tolerance: 0,
        onDrop: function (item, container, _super) {
            var data = $( "#console" ).sortable("serialize").get();
            var images = JSON.stringify(data, null, ' ');

            updateImagePosition(images);

            _super(item, container)

        },
        onDragStart: function ($item, container, _super, event ) {
            oldPosition = $item.index();

            $item.css({
                height: $item.height(),
                width: $item.width()
            })
            $item.addClass("dragged")
            $("body").addClass("dragging")
        },
        onDrag: function ($item, position) {
            $item.css(position)
        }
    });
    $( "#console" ).disableSelection();
}

function init_delete_images() {
    $('.delete_image').click(function()
    {
        var object = $(this).parent().attr('data-object');
        var id = $(this).parent().attr('data-id');
        var elem = $(this);

        $.ajax({
            url: urlApiDeleteImage,
            dataType: 'json',
            type: 'POST',
            data:
            {
                object: object,
                id: id,
            },
            beforeSend:function(d)
            {
                $(elem).html("<img src='/bundles/footprinttravelbackoffice/img/loader_small.gif' id='loader' />");
                $(elem).css('left', '51px');
                $(elem).css('opacity', '0.8');
            },
            error: function(d)
            {
                $(elem).html("<span class='glyphicon glyphicon-remove'></span>");
                $(elem).css('left', '63px');
                $(elem).css('opacity', '0.5');
            },
            success:function(d)
            {
                $(elem).parent().remove();
            }
        })
    });
}

function updateImagePosition(images) {
    $.ajax({
        url: urlApiUpdateImagePosition,
        dataType: 'json',
        type: 'POST',
        data:
        {
            images: images
        },
        beforeSend:function(d)
        {
            $( "#console" ).css("opacity", "0.5");
        },
        error:function(d)
        {
            $( "#console" ).css("opacity", "1");
        },
        success:function(d)
        {
            $( "#console" ).css("opacity", "1");
        }
    });
}


function initEditableImages() {
    /*
    if($('.image_title').length) {
        $('.image_title').editable({
            emptytext: 'Klik om te bewerken'
        });
    }
    */
}

function updateOptionValue(elem) {
    $.ajax({
        url: $(elem).data('url'),
        data: {
            name: $(elem).data('field'),
            value: $(elem).find('option:selected').val(),
            pk: $(elem).data('pk')
        },
        beforeSend: function() {
            $('#loader').show();
        },
        success: function(d) {
            $('#loader').hide();
        },
        error: function(d) {
        }
    });
}