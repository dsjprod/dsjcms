
var page = function () {
    function loadDeleteButton(){
        $('.delete_row').on('click', function(e) {
            e.preventDefault();

            if(window.confirm('Weet je zeker dat je deze regel wilt verwijderen?'))
            {
                var id = $(this).data('id');
                $('#pageRow_'+id).remove();
            }
            $('#row_'+id).remove();
        });

        $('.delete_content').on('click', function(e) {
            e.preventDefault();

            if(window.confirm('Weet je zeker dat je dit contentblok wilt verwijderen?'))
            {
                var content = $(this).data('content');
                var row = $(this).data('row');
                $('.contentitem'+row+'_'+content).remove();
            }
        });
    }

    function initSetContentTypeButtons(){
        $('.contentEditButton').on('click', function(e) {
            var href = $(this).attr('href');
            var modal = $(href);
            var type = $(this).data('type');
            changeModalContent(modal, type);
        });

        $('.set_contenttype').on('click', function(e) {
            e.preventDefault();
            var typeId = $(this).data('id');
            $(this).closest(".modal").find(".contentTypeId").attr("value", typeId);

            changeModalContent($(this).closest(".modal"), typeId);
        });
    }

    function changeModalContent(modal, typeId){
        modal.find("a.active").removeClass("active");
        modal.find("a[data-id='"+typeId+"']").addClass("active");
        modal.find('.form-group').hide();

        $.ajax({
            url: sUrlApiGetContentType,
            data: {
                id: typeId
            },
            dataType: 'json',
            beforeSend: function() {
                $("#loader").fadeIn();
            },
            success: function (d) {
                modal.find(".contentTitle").closest('.form-group').show();
                modal.find(".contentBackendTitle").closest('.form-group').show();
                modal.find(".contentActive").closest('.form-group').show();

                for (index = 0; index < d.fields.length; ++index) {
                    if(d.fields[index] !== "") {
                        modal.find("." + d.fields[index]).closest('.form-group').show();
                    }
                }
                $("#loader").fadeOut();
            }
        });
        /*
        switch(typeId)
        {
            case 2:
                modal.find(".contentContent").closest('.form-group').show();
                if (CKEDITOR.instances[modal.find(".contentContent").attr('id')]) {
                    CKEDITOR.instances[modal.find(".contentContent").attr('id')].destroy();
                }
                CKEDITOR.replace(modal.find(".contentContent").attr('id'));
                modal.find(".contentImage").closest('.form-group').show();
                modal.find(".contentImageAlign").closest('.form-group').show();
                modal.find(".form").closest('.form-group').hide();
                break;
            case 3:
                modal.find(".contentContent").closest('.form-group').hide();
                modal.find(".contentImage").closest('.form-group').show();
                modal.find(".contentImageAlign").closest('.form-group').show();
                modal.find(".form").closest('.form-group').hide();
                break;
            case 4:
                modal.find(".contentContent").closest('.form-group').show();
                var editor = CKEDITOR.instances[modal.find(".contentContent").attr('id')]; if(typeof editor !== "undefined") { editor.destroy(true); }
                modal.find(".contentImage").closest('.form-group').hide();
                modal.find(".contentImageAlign").closest('.form-group').hide();
                modal.find(".form").closest('.form-group').hide();
                break;
            case 5:
                modal.find(".contentContent").closest('.form-group').hide();
                modal.find(".contentImage").closest('.form-group').hide();
                modal.find(".contentImageAlign").closest('.form-group').hide();
                modal.find(".form").closest('.form-group').show();
                break;
            case 6:
                modal.find(".contentContent").closest('.form-group').show();
                if (CKEDITOR.instances[modal.find(".contentContent").attr('id')]) {
                    CKEDITOR.instances[modal.find(".contentContent").attr('id')].destroy();
                }
                CKEDITOR.replace(modal.find(".contentContent").attr('id'));
                modal.find(".contentImage").closest('.form-group').hide();
                modal.find(".contentImageAlign").closest('.form-group').hide();
                modal.find(".form").closest('.form-group').show();
                break;
            case 7:
                modal.find(".contentContent").closest('.form-group').hide();
                modal.find(".contentImage").closest('.form-group').hide();
                modal.find(".contentImageAlign").closest('.form-group').hide();
                modal.find(".form").closest('.form-group').hide();
                break;
            default:
            case 1:
                modal.find(".contentContent").closest('.form-group').show();
                if (CKEDITOR.instances[modal.find(".contentContent").attr('id')]) {
                    CKEDITOR.instances[modal.find(".contentContent").attr('id')].destroy();
                }
                CKEDITOR.replace(modal.find(".contentContent").attr('id'));
                modal.find(".contentImage").closest('.form-group').hide();
                modal.find(".contentImageAlign").closest('.form-group').hide();
                modal.find(".form").closest('.form-group').hide();
                break;
        }
        */
    }

    function initAddContentButtons(){
        $('.add_content').on('click', function(e) {
            e.preventDefault();

            var pageRow = $(this).data('row');
            var size = $(this).data('size');

            $collectionHolderContent = $('div.contentContainer_'+pageRow);
            if($collectionHolderContent.data('prototype') == null){
                $collectionHolderContent = $('div#dsj_cms_pagepagerow_pageRows_'+pageRow+'_pageContent');
            }
            $collectionIndexHolderContent = $('div#row_'+pageRow);
            $collectionHolderContent.data('index', $collectionIndexHolderContent.children().length);
            addContentForm($collectionHolderContent, pageRow, size);
        });
    }

    function addContentForm($collectionHolder, pageRow, size) {
        var prototype = $collectionHolder.data('prototype');
        prototype = prototype.replace('[pageContent][__name__][size]" class', '[pageContent][__name__][size]" value="'+size+'" class');
        prototype = prototype.replace('[pageContent][__name__][type]" class', '[pageContent][__name__][type]" value="1" class');
        var index = $collectionHolder.data('index');

        var newForm = prototype.replace(/__name__/g, index);
        var preForm = '<div class="modal container fade contentitem'+pageRow+'_' + index + '" id="row_'+pageRow+'_contentModal_' + index + '" role="basic" aria-hidden="true" style="display: none; margin-top: 50px; overflow: visible;"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+newContentTypeList+'<h4 class="modal-title">Contentblok bewerken</h4></div><div class="modal-body">';
        var postForm = '</div><div class="modal-footer"><button type="button" class="btn default" data-dismiss="modal">Sluiten</button></div></div></div>';

        var button = '<div id="contentitem'+pageRow+'_' + index + '" class="col-md-'+size+' contentitem contentitem'+pageRow+'_' + index + '" data-size="'+size+'"><div class="panel panel-default"><div class="panel-body"><div class="actions pull-right"><div class="btn-group"><a href="#" data-type="" class="btn yellow sort-handle"><i class="fa fa-arrows"></i></a><a href="#row_'+pageRow+'_contentModal_' + index + '" data-toggle="modal" data-type="1" class="btn blue contentEditButton"><i class="fa fa-pencil"></i></a><a href="#" data-content="' + index + '" data-row="'+pageRow+'" class="btn red delete_content"><i class="fa fa-trash-o"></i></a></div></div>Nieuw</div></div></div>';

        $collectionHolder.data('index', index + 1);
        $('div.contentContainer_'+pageRow).append(button);
        $('#row_'+pageRow+'.content_modals').append(preForm + newForm + postForm);
        $('#dsj_cms_pagepagerow_pageRows_'+pageRow+'_pageContent_'+index+'_sequence').val(index);

        CKEDITOR.replace('dsj_cms_pagepagerow_pageRows_'+pageRow+'_pageContent_'+index+'_content', {customConfig: ''});
        $('#dsj_cms_pagepagerow_pageRows_'+pageRow+'_pageContent_'+index+' .make-switch')['bootstrapSwitch']();
        loadDeleteButton();
        initSetContentTypeButtons();
    }

    var initAddRowButton = function() {


        $('.add_row').on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // add a new tag form (see next code block)
            addRowForm($collectionHolderpageRows);
            initAddContentButtons();
        });
    }

    function addRowForm($collectionHolder) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = prototype.replace(/__name__/g, index);
        var newForm = newForm.replace('make-switch', 'make-switch'+index);
        var preForm = '<div id="pageRow_'+ index +'" class="col-md-12 page-row"><div class="portlet box grey pageRow"><div class="portlet-title"><div class="caption"><i class="fa fa-reorder"></i>Pagina regel</div><div class="actions"><a class="btn yellow sort-handle" href="#" onclick="return false;" style="margin-right:3px;"><i class="fa fa-arrows"></i> Verplaatsen</a><div class="btn-group"><a class="btn green" href="#" data-toggle="dropdown"><i class="fa fa-pencil"></i> Item toevoegen <i class="fa fa-angle-down "></i></a><ul class="dropdown-menu pull-right"><li><a href="#" class="add_content" data-size="4" data-row="'+ index +'">1/3 breedte</a></li><li><a href="#" class="add_content" data-size="6" data-row="'+ index +'">1/2 breedte</a></li><li><a href="#" class="add_content" data-size="8" data-row="'+ index +'">2/3 breedte</a></li><li><a href="#"class="add_content" data-size="12" data-row="'+ index +'">1/1 breedte</a></li></ul></div> <a href="#" class="btn red delete_row" data-id="' + index + '"><i class="fa fa-trash-o"></i> Verwijderen</a></div></div><div class="portlet-body form-horizontal">';
        var postForm = '<div class="row contentContainer_'+index+'"></div></div></div></div>';

        var modalcontainer = '<div id="row_'+index+'" class="content_modals"></div>'

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);
        $('#dsj_cms_pagepagerow_pageRows_'+index+'_sequence').val(index);

        // Display the form in the page in an li, before the "Add a tag" link li
        //var $newFormLi = $('<li></li>').append(newForm);
        //$newLinkLi.before($newFormLi);
        $('#pageRows').append(preForm + newForm + postForm);
        $('#AllContentModals').append(modalcontainer);
        handleColorSelect();
        loadDeleteButton();
    }

    function initSortableRowsAndContentItems()
    {
        // make content items sortable
        $(".row.contentItems").sortable({
            handle:".sort-handle",
            placeholder: "sortable-placeholder",
            cursor: "move",
            start: function(e, ui)
            {
                ui.placeholder.height(ui.item.find('.panel').height());
                ui.placeholder.width(ui.item.find('.panel').width());
                ui.placeholder.attr('class', 'sortable-placeholder '+ui.item.attr('class'));
            },
            update: function(event, ui)
            {
                var aSortedArray = $(ui.item).closest('.row.contentItems').sortable("toArray");
                for(i=0;i<aSortedArray.length;i++)
                {
                    var aItemArray = aSortedArray[i].replace('contentitem','').split('_');
                    var iRow = aItemArray[0];
                    var iContentItem = aItemArray[1];
                    $('#dsj_cms_pagepagerow_pageRows_'+iRow+'_pageContent_'+iContentItem+'_sequence').val(i);
                }
            }
        });

        // make page rows sortable
        $("#pageRows").sortable({
            handle:".sort-handle",
            cursor: "move",
            placeholder: "sortable-placeholder col-md-12",
            start: function(e, ui)
            {
                ui.placeholder.width(ui.item.find('.portlet.box').width());
                ui.placeholder.height(ui.item.find('.portlet.box').height());
            },
            update: function(event, ui)
            {
                var aSortedArray = $(ui.item).parent().sortable("toArray");
                for(i=0;i<aSortedArray.length;i++)
                {
                    var iRow = aSortedArray[i].replace('pageRow_','');
                    $('#dsj_cms_pagepagerow_pageRows_'+iRow+'_sequence').val(i);
                }
            }
        });
    }

    var handleColorSelect = function () {
        $('.bg-color').each(function() {
            $(this).css('background-color', $(this).find('option:selected').attr("data-bg-color"));
            $(this).css('color', $(this).find('option:selected').attr("data-text-color"));
        });
        $('.bg-color').change(function(){
            $(this).css('background-color', $(this).find('option:selected').attr("data-bg-color"));
            $(this).css('color', $(this).find('option:selected').attr("data-text-color"));
        });
    }

    var initAjaxForm = function () {
        $("#page_pagecontent_form").submit(function(e) {
            e.preventDefault(true);
            updateCkeditors();
            for ( instance in CKEDITOR.instances ) {
                $('#'+instance).html(CKEDITOR.instances[instance].getData());
            }
            submitPageContentForm();
            return false;
        });

    }

    return {
        //main function to initiate the theme
        init: function () {
            initAjaxForm();

            loadDeleteButton();
            loadLocaleButtons();
            initAddRowButton();
            initAddContentButtons();
            initSetContentTypeButtons();
            handleColorSelect();

            initSortableRowsAndContentItems();

            $('#dsj_cms_pagepagerow_newRow').click(function()
            {
                $('#dsj_cms_pagepagerow_addRow').val(1);
            });

            $collectionHolderpageRows = $('#pageRows');
            $collectionHolderpageRows.data('index', $collectionHolderpageRows.children().length);

            CKFinder.setupCKEditor( null, '/bundles/dsjcmsbackoffice/assets/plugins/ckfinder/' );
        }
    }
}();

function submitPageContentForm()
{
    $("#page_pagecontent_form").ajaxSubmit({
    dataType: 'json',
    data: $('#page_pagecontent_form').formSerialize(),
    beforeSend: function() {
        $("#loader").fadeIn();
    },
    success: function(r, s, xhr, $form) {
        $form.find(".contentContent").each(function() {
            if (CKEDITOR.instances[$(this).attr('id')]) {
                CKEDITOR.instances[$(this).attr('id')].destroy()
            }
        });

        $form.parent().html(r.response);
        $('.make-switch').bootstrapSwitch();
        page.init();
        ckeditor.init();

        $("#loader").fadeOut();
    }
});
}