var oSeoTitleElem;
var oSeoDescrElem;
var iSeoTitleMax = 70;
var iSeoDescrMax = 156;

$(function()
{
    // seo title
    $('input,textarea').each(function()
    {
        if(typeof($(this).attr('name')) != 'undefined') {
            if ($(this).attr('name').indexOf('[seoTitle]') > -1) {
                oSeoTitleElem = $(this);
                $(this).keyup(function () {
                    renderSeoSnippet();
                });
            }
            else {
                if ($(this).attr('name').indexOf('[seoDescription]') > -1) {
                    oSeoDescrElem = $(this);
                    $(this).keyup(function () {
                        renderSeoSnippet();
                    });
                }
            }
        }
    });
    if(typeof(oSeoTitleElem) != 'undefined' && typeof(oSeoDescrElem) != 'undefined')
    {
        window.setInterval(function(){
            renderSeoSnippet();
        },500);
    }
});

function renderSeoSnippet()
{
    if($(oSeoDescrElem).parent().find('.seo-google-snippet').length == 0)
    {
        $(oSeoDescrElem).parent().append('<div class="seo-google-snippet">' + $('.seo-google-snippet').html() + '</div>');
        $(oSeoDescrElem).parent().find('.seo-google-snippet').slideDown();
        $(oSeoTitleElem).parent().append('<span class="badge badge-success seo-badge seo-badge-title">'+iSeoTitleMax+'</span>');
        $(oSeoDescrElem).parent().append('<span class="badge badge-success seo-badge seo-badge-descr">'+iSeoDescrMax+'</span>');
    }

    var oSeoSnippet = $(oSeoDescrElem).parent().find('.seo-google-snippet');
    if(typeof(oSeoTitleElem) != 'undefined' && $(oSeoTitleElem).val() != '')
    {
        $(oSeoSnippet).find('.title').text($(oSeoTitleElem).val());
        if($(oSeoTitleElem).val().length > iSeoTitleMax)
        {
            $(oSeoTitleElem).addClass('warning');
            $(oSeoTitleElem).parent().find('.seo-badge-title').addClass('badge-danger');
        }
        else
        {
            $(oSeoTitleElem).removeClass('warning');
            $(oSeoTitleElem).parent().find('.seo-badge-title').removeClass('badge-danger');
        }
        $(oSeoTitleElem).parent().find('.seo-badge-title').text(iSeoTitleMax - $(oSeoTitleElem).val().length);
    }
    if(typeof(oSeoDescrElem) != 'undefined' && $(oSeoDescrElem).val() != '')
    {
        $(oSeoSnippet).find('.description').text($(oSeoDescrElem).val());
        if($(oSeoDescrElem).val().length > iSeoDescrMax)
        {
            $(oSeoSnippet).find('.description').text($(oSeoDescrElem).val().substr(0,(iSeoDescrMax-4))+' ...');
            $(oSeoDescrElem).addClass('warning');
            $(oSeoDescrElem).parent().find('.seo-badge-descr').addClass('badge-danger');
        }
        else
        {
            $(oSeoDescrElem).removeClass('warning');
            $(oSeoDescrElem).parent().find('.seo-badge-descr').removeClass('badge-danger');
        }
        $(oSeoDescrElem).parent().find('.seo-badge-descr').text(iSeoDescrMax - $(oSeoDescrElem).val().length);
    }
}
