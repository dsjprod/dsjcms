var currentMousePos = { x: -1, y: -1 };
function initSortable(elem, nested, vertical) {
    var oldContainer;
    $(elem).sortable({
        exclude: '.not-sortable',
        scroll: true,
        vertical: vertical,
        nested: nested,
        onDrop: function (item, container, _super) {
            stopScrolling();
            if(nested == true) {
                var parent_id = container.el.data('parent-id');
            } else {
                var parent_id = container.el.data('parent-id');
            }

            var children = new Array();
            item.parent().children().each(function() {
                children.push($(this).data('id'));
            })

            $.ajax({
                url: urlUpdateSequence,
                data: {
                    newParentId: parent_id,
                    children: children,
                    entity: sEntity
                },
                dataType: "json",
                beforeSend:function(d)
                {
                    $(elem).fadeTo("slow", 0.4);
                },
                success:function(d)
                {
                    $(elem).fadeTo("slow", 1);
                    if(d !== true) {
                        alert('Volgorde opslaan niet gelukt! Probeer het opnieuw');
                    }
                }
            });
            _super(item)
        },
        onDrag: function (item, position, _super) {
            $(document).mousemove(function(event) {
                currentMousePos.y = event.pageY;
            });

            if(currentMousePos.y < ($(".header.navbar").offset().top+$(".header.navbar").height()+20)) {
                startScrolling($("body"), 'up');
            } else if(currentMousePos.y > ($(window).scrollTop()+$(window).height()-20)) {
                startScrolling($("body"), 'down');
            } else {
                stopScrolling();
            }
        }
    });
}

function initDeletable() {
    $('.delete_sequence').click(function()
    {
        if(window.confirm('Weet je zeker dat je deze wilt verwijderen uit de lijst?')) {
            var sequence_id = $(this).parent().data('id');
            var elem = $(this);

            $.ajax({
                url: urlRemoveSequence,
                dataType: 'json',
                data:
                {
                    sequence_id: sequence_id
                },
                success: function(d) {
                    $(elem).parent().slideUp().delay(500).remove();
                }
            });
        } else {
            return false;
        }
    });
}

function toggleSlide(elem) {
    $(elem).siblings('ol').slideToggle();
    if($(elem).children('i.fa').hasClass('fa-minus-square')) {
        $(elem).children('i.fa').removeClass('fa-minus-square').addClass('fa-plus-square');
    } else {

        $(elem).children('i.fa').removeClass('fa-plus-square').addClass('fa-minus-square');
    }
}


var scrollFunction=null;

function startScrolling($scrollable, scroll){
    if(scrollFunction == null) {
        scrollFunction = setInterval(function(){
            if (scroll == 'up') {
                $scrollable.scrollTop($scrollable.scrollTop()-20);
            }
            else if (scroll == 'down'){
                $scrollable.scrollTop($scrollable.scrollTop()+20);
            }
        }, 50);
    }
}

function stopScrolling() {
    clearInterval(scrollFunction);
    scrollFunction=null;
}