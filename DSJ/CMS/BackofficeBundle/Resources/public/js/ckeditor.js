var ckeditorContent = new Array();

var ckeditor = function () {
    var initEditors = function() {
        if($(".ckeditor").length > 0) {
            $.ajax({
                url: sessionStatusUrl
            });

            CKFinder.setupCKEditor( null, '/bundles/dsjcmsbackoffice/assets/plugins/ckfinder/' );
            $("textarea.ckeditor").each(function()
            {
                if((!($(this).attr('id') in ckeditorContent) || ckeditorContent.length == 0) && !($(this).attr('id') in CKEDITOR.instances))
                {
                    eval("ckeditorContent['" + $(this).attr('id') + "'] = CKEDITOR.replace(" + $(this).attr('id') + ");");
                }
            });
        }
    }

    return {
        //main function to initiate the theme
        init: function () {
            initEditors();
        },

        reloadCkeditor: function() {
            $('.ckeditor').each(function () {
                if($(this).attr('id') in ckeditorContent) {
                    ckeditorContent[$(this).attr('id')].destroy(true);
                }
            });
        }
    }
}();

function updateCkeditors()
{
    for ( instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
    }
}

$(function() {
    ckeditor.init();
});