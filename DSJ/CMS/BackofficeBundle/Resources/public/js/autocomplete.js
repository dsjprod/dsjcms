var entities = Array();
var entitiesString = Array();
var elems = Array();

function addForm($collectionHolder) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    $collectionHolder.append(newForm);
    initDeleteButton($collectionHolder.find('div.collection-item').last());
}

var initAddButton = function($collectionHolder, initAutocomplete) {
    $collectionHolder.data('index', $collectionHolder.find('div.collection-item').length);
    $collectionHolder.siblings('a.add').on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addForm($collectionHolder);
        if(initAutocomplete == true) {
            autocomplete.init();
        }
    });
}

var initDeleteButton = function($element) {
    var $removeFormA = $('<a href="#" class="remove btn red btn-sm"><i class="fa fa-times"></i></a>');
    $element.append($removeFormA);

    $removeFormA.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // remove the li for the tag form
        $element.remove();
    });
}

var autocomplete = function () {
    return {
        init: function () {
            $('.typeahead').each(function(i) {
                initSingleTypeahead($(this), i);
            });
        },
        initElem: function(elem) {
            initSingleTypeahead($(elem), entities.length);
        }
    };
}();

function initSingleTypeahead(elem, i) {
    $(elem).typeahead('destroy');
    elems.push($(elem));
    var url = urlAPIAutocomplete+'?object='+$(elem).data('class')+'&search=%QUERY';
    var config = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: url,
            filter: function (object) {
                return $.map(object, function (o) {
                    return {
                        value: o.name
                    };
                });
            },
            ajax: {
                beforeSend: function(){
                    showSpinningWheel(elems[i]);
                },
                complete: function() {
                    hideSpinningWheel(elems[i]);
                }
            }
        }
    });

    if (entitiesString.indexOf(i) == -1){
        eval('entities.push({'+i+': config});');
        entitiesString.push(i);

    }
    entities[$.inArray(i, entitiesString)][i].initialize();

    $(elem).typeahead(null, {
        displayKey: 'value',
        source: entities[$.inArray(i, entitiesString)][i].ttAdapter()
    });
}

function showSpinningWheel($elem) {
    $elem.addClass('spinner');
}

function hideSpinningWheel($elem) {
    $elem.removeClass('spinner');
}