function init_autocomplete()
{
    if($(".autocomplete_input").length > 0)
    {
        /* Autocomplete for searching */
        $(".autocomplete_input").each(function()
        {
            $(this).autocomplete({
                source: function(request, callback)
                {
                    var elem = $(this.element);
                    var autocompleteMethod=null;
                    if($(this.element).data('autocomplete-custom-method')) {
                        var autocompleteMethod=$(this.element).data('autocomplete-custom-method');
                    }
                    $.getJSON(urlAPIAutocomplete, {search: request.term, object: $(this.element).data('class'), method: autocompleteMethod}, callback);
                },
                minLength: 2,
                search: function( event, ui ) {
                    $(this).addClass('spinner');
                },
                response: function( event, ui ) {
                    $(this).removeClass('spinner');
                },
                select: function( event, ui ) {
                    if(ui.item)
                    {
                        if($(this).parent().find('.autocomplete_widget').attr('multiple') == undefined) {
                            $(this).parent().find('.autocomplete_list').children().remove();
                            $(this).parent().find('.autocomplete_widget').find('option').attr('selected', false);
                        }
                        $(this).parent().find('.autocomplete_list').append('<li class="autocomplete_item not-sortable">'+ui.item.name+'&nbsp;<span class="btn btn-danger delete_autocomplete" data-autocomplete-id="'+ui.item.id+'"><span class="glyphicon glyphicon-remove"></span></span></li>');
                        $(this).parent().find('.autocomplete_widget').val(ui.item.id);
                        init_autocomplete_actions();
                    }
                }
            });
        });
    }

    if($(".autocomplete_collection_input").length > 0)
    {
        /* Autocomplete for searching */
        $(".autocomplete_collection_input").each(function()
        {
            $(this).autocomplete({
                source: function(request, callback)
                {
                    var elem = $(this.element);
                    var autocompleteMethod=null;
                    if($(this.element).data('autocomplete-custom-method')) {
                        var autocompleteMethod=$(this.element).data('autocomplete-custom-method');
                    }
                    $.getJSON(urlAPIAutocomplete, {search: request.term, object: $(this.element).data('class'), method: autocompleteMethod}, callback);
                },
                minLength: 2,
                search: function( event, ui ) {
                    $(this).addClass('spinner');
                },
                response: function( event, ui ) {
                    $(this).removeClass('spinner');
                },
                select: function( event, ui ) {
                    if(ui.item)
                    {
                        $(this).parent().find('.autocomplete_list').append('<li class="autocomplete_item not-sortable">'+ui.item.name+'&nbsp;<span class="btn red delete_autocomplete_collection btn-sm" data-autocomplete-id="'+ui.item.id+'" title="Verwijderen"><i class="fa fa-trash-o"></i></span></li>');
                        var collectionHolder = $(this).parent().find('.autocomplete_collection_widget');
                        var prototype = $(collectionHolder).data('prototype');
                        var index = $(collectionHolder).find('select').length;
                        var newForm = prototype.replace(/__name__/g, index);
                        var selectId = $(newForm).find('select').attr('id');

                        $(collectionHolder).append($(newForm));
                        $('#'+selectId).find('option[value="'+ui.item.id+'"]').attr('selected', 'selected');

                        init_autocomplete_actions();
                    }
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a>" + item.name + "</a>" )
                    .appendTo( ul );
            };
        });
    }
}

function init_autocomplete_actions()
{
    if($('.delete_autocomplete_collection').length > 0) {
        $('.delete_autocomplete_collection').click(function () {
            var index = $(this).parent().index();
            $(this).parent().parent().parent().parent().find('.autocomplete_collection_widget,.autocomplete_single_widget').children(':nth-child(' + (index + 1) + ')').remove();
            $(this).parent().slideUp().delay(800).remove();

            if ($(this).data("class")) {
                var object = $(this).data("class");
                var autocomplete_id = $(this).data("autocomplete-id");
                var url = $(this).data("url");
                $.ajax({
                    url: url,
                    dataType: 'json',
                    data: {
                        object: object,
                        autocomplete_id: autocomplete_id
                    }
                })
            }
        });
    }
}