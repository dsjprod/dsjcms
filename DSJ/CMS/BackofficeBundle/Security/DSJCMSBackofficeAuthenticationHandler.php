<?php

namespace DSJ\CMS\BackofficeBundle\Security;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Cookie;
use Doctrine\Bundle\DoctrineBundle\Registry;


class DSJCMSBackofficeAuthenticationHandler
implements AuthenticationSuccessHandlerInterface
{

    /**
     * @var Router $oRouting
     */
    public $oRouting;

    /**
     * @var Registry $oEm
     */
    public $oEm;
    
    public  function __construct($oRouting, Registry $oDoctrine) {
        $this->oRouting = $oRouting;
        $this->oEm = $oDoctrine->getManager();
    }


    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request        $oRequest
     * @param TokenInterface $token
     *
     * @return Response the response to return
     */
    function onAuthenticationSuccess(Request $oRequest, TokenInterface $token)
    {

        if(null !== $oRequest->getSession()->get('_security.admin.target_path') && !strstr($oRequest->getSession()->get('_security.admin.target_path'), 'favicon.ico')) {
            $oResponse = new RedirectResponse($oRequest->getSession()->get('_security.admin.target_path'));
        } else {
            $oResponse = new RedirectResponse($this->oRouting->generate('dsj_cms_index'));
        }

        $oResponse->headers->setCookie(new Cookie('store24backoffice_lastUsername',$token->getUser()->getUsername()));
        return $oResponse;
    }
}
