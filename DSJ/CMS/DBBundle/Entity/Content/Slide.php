<?php

namespace DSJ\CMS\DBBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Translatable\Translatable;

/**
 * DSJ\CMS\DBBundle\Entity\Content\Slide
 *
 * @ORM\MappedSuperclass
 * @ORM\Table(name="slide")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"backend" = "DSJ\CMS\DBBundle\Entity\Content\Slide", "frontend" = "DSJ\CMS\FrontendDBBundle\Entity\Content\Slide"})
 * @ORM\Entity(repositoryClass="DSJ\CMS\DBBundle\Entity\Content\Repository\SlideRepository")
 * @Gedmo\TranslationEntity(class="DSJ\CMS\DBBundle\Entity\Content\Translation\SlideTranslation")
 * @Vich\Uploadable
 */
class Slide implements Translatable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $title
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;
    
    /**
     * @var string $backendTitle
     *
     * @ORM\Column(name="backend_title", type="string", length=255, nullable=true)
     */
    protected $backendTitle;
    
    /**
     * @var string $subtitle
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    protected $subtitle;

    /**
     * @var string $link
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    protected $link;

    /**
     * @var string $buttonText
     *
     * @ORM\Column(name="button_text", type="string", length=255, nullable=true)
     */
    protected $buttonText;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;

    /**
     * @Vich\UploadableField(mapping="slide_image", fileNameProperty="image")
     */
    protected $imageFile;
    
    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    protected $active;
    
    /**
     * @var integer $sequence
     *
     * @ORM\Column(name="sequence", type="integer")
     */
    private $sequence;

    /**
     * Post locale
     * Used locale to override Translation listener's locale
     *
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="date")
     */
    protected $created;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updated;

    public function __toString() {
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        if(null===$title){$title=' ';}
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set link
     *
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set created
     *
     * @param date $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return date 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set image
     *
     * @param Store24\DBBundle\Entity\System\SingleImage $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * Get image
     *
     * @return Store24\DBBundle\Entity\System\SingleImage 
     */
    public function getImage()
    {
        return $this->image;
    }

    public function setImageFile($imageFile)
    {
    	$this->imageFile = $imageFile;
    	if ($this->imageFile) {
    		$this->updated = new \DateTime('now');
    	}
    }
    
    /**
     * @return string
     */
    public function getImageFile()
    {
    	return $this->imageFile;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return HomepageSlide
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return HomepageSlide
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set subtitle
     *
     * @param string $subtitle
     * @return HomepageSlide
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    
        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string 
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Set backendTitle
     *
     * @param string $backendTitle
     * @return HomepageSlide
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    
        return $this;
    }

    /**
     * Get backendTitle
     *
     * @return string 
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Set buttonText
     *
     * @param string $buttonText
     * @return Slide
     */
    public function setButtonText($buttonText)
    {
        $this->buttonText = $buttonText;

        return $this;
    }

    /**
     * Get buttonText
     *
     * @return string 
     */
    public function getButtonText()
    {
        return $this->buttonText;
    }
}
