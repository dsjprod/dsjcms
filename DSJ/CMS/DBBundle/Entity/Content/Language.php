<?php

namespace DSJ\CMS\DBBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Language
 *
 * @ORM\Table(name="language")
 * @ORM\Entity()
 */
class Language {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $language
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $language;

    /**
     * @var string $code
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $code;
    
    /**
     * @var string $code_long
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $code_long;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Language
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Language
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
    
    /**
     * Set code_long
     *
     * @param string $code_long
     * @return Language
     */
    public function setCodeLong($code_long)
    {
        $this->code_long = $code_long;

        return $this;
    }

    /**
     * Get code_long
     *
     * @return string 
     */
    public function getCodeLong()
    {
        return $this->code_long;
    }
}
