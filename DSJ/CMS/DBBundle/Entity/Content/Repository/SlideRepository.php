<?php

namespace DSJ\CMS\DBBundle\Entity\Content\Repository;

use DSJ\CMS\DBBundle\Entity\Repository\TranslatableRepository;

/**
 * SlideRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SlideRepository extends TranslatableRepository
{
    public function getFiltered($iLimit=null, $iStart=0, $aColumns=array(), $sSearch=null, $aOrder=array())
    {
        $aJoinedObjects = array();

        $oQb =  $this->getEntityManager()
            ->createQueryBuilder()
            ->select("p")
            ->from("DSJCMSDBBundle:Content\Slide", "p");

        $sRelations = "aa";
        $sWhere = "";
        if(sizeof($aColumns)>0 && $sSearch !== NULL) {
            foreach ($aColumns as $iKey => $sColumn) {
                if($iKey !== 0) {
                    $sWhere .= " OR ";
                }
                if(is_array($sColumn)) {
                    $sWhere .= 'CONCAT(';
                    foreach($sColumn as $iKey2 => $sColumn2) {
                        if (stristr($sColumn2, '.') !== false) {
                            $aRelation = explode('.', $sColumn2);
                            $i = 0;
                            $sPrevObject = 'p';
                            while ($i < count($aRelation) - 1) {
                                if(!array_key_exists($aRelation[$i], $aJoinedObjects)) {
                                    $aJoinedObjects[$aRelation[$i]] = $sRelations;
                                    $oQb->leftJoin($sPrevObject . '.' . $aRelation[$i], $sRelations);
                                    $sPrevObject = $sRelations;
                                    $sRelations++;
                                } else {
                                    $sPrevObject = $aJoinedObjects[$aRelation[$i]];
                                }
                                $i++;
                            }
                            $sWhere .= ($iKey2 !== 0 ? ', CONCAT(\' \', '. ($iKey2 != (sizeof($sColumn)-1) ? 'CONCAT(' : '') : '') . 'COALESCE (' . $sPrevObject . '.' . $aRelation[count($aRelation) - 1] .',\'\')';
                        } else {
                            $sWhere .= 'p.' . $sColumn2;
                        }
                    }
                    for($j=0;$j<count($sColumn)-1;$j++) {
                        $sWhere .= '))';
                    }
                    $sWhere .= ' LIKE :search';
                } else {
                    if (stristr($sColumn, '.') !== false) {
                        $aRelation = explode('.', $sColumn);
                        $i = 0;
                        $sPrevObject = 'p';
                        while ($i < count($aRelation) - 1) {
                            if(!array_key_exists($aRelation[$i], $aJoinedObjects)) {
                                $aJoinedObjects[$aRelation[$i]] = $sRelations;
                                $oQb->leftJoin($sPrevObject . '.' . $aRelation[$i], $sRelations);
                                $sPrevObject = $sRelations;
                                $sRelations++;
                            } else {
                                $sPrevObject = $aJoinedObjects[$aRelation[$i]];
                            }
                            $i++;
                        }
                        $sWhere .= $sPrevObject . '.' . $aRelation[count($aRelation) - 1] . ' LIKE :search OR ' . $sPrevObject . '.' . $aRelation[count($aRelation) - 1] . ' LIKE :search_literal';
                    } else {
                        $sWhere .= 'p.' . $sColumn . ' LIKE :search OR p.' . $sColumn . ' = :search_literal';
                    }
                }
            }
            $oQb->setParameter('search', '%' . $sSearch . '%');
            $oQb->setParameter('search_literal', $sSearch);
            $oQb->andWhere($sWhere);
        }

        foreach($aOrder as $sOrderField => $sOrderDir)
        {
            $sOrder = '';
            if (stristr($sOrderField, '.') !== false) {
                $aRelation = explode('.', $sOrderField);
                $i = 0;
                $sPrevObject = 'p';
                while ($i < count($aRelation) - 1) {
                    if(!array_key_exists($aRelation[$i], $aJoinedObjects)) {
                        $aJoinedObjects[$aRelation[$i]] = $sRelations;
                        $oQb->leftJoin($sPrevObject . '.' . $aRelation[$i], $sRelations);
                        $sPrevObject = $sRelations;
                        $sRelations++;
                    } else {
                        $sPrevObject = $aJoinedObjects[$aRelation[$i]];
                    }
                    $i++;
                }
                $sOrder .= $sPrevObject . '.' . $aRelation[count($aRelation) - 1];
            } else {
                $sOrder .= 'p.' . $sOrderField;
            }
            $oQb->addOrderBy($sOrder, $sOrderDir);
        }

        if($iLimit != null) {
            $oQb
                ->setFirstResult($iStart)
                ->setMaxResults($iLimit);
        }
        $aResult = $oQb->getQuery()
            ->getResult();
        return $aResult;
    }


    public function getFilteredCount($aColumns=array(), $sSearch=null, $aOrder=array())
    {
        $aJoinedObjects = array();

        $oQb =  $this->getEntityManager()
            ->createQueryBuilder()
            ->select("COUNT(p)")
            ->from("DSJCMSDBBundle:Content\Slide", "p");

        $sRelations = "aa";
        $sWhere = "";
        if(sizeof($aColumns)>0 && $sSearch !== NULL) {
            foreach ($aColumns as $iKey => $sColumn) {
                if($iKey !== 0) {
                    $sWhere .= " OR ";
                }
                if(is_array($sColumn)) {
                    $sWhere .= 'CONCAT(';
                    foreach($sColumn as $iKey2 => $sColumn2) {
                        if (stristr($sColumn2, '.') !== false) {
                            $aRelation = explode('.', $sColumn2);
                            $i = 0;
                            $sPrevObject = 'p';
                            while ($i < count($aRelation) - 1) {
                                if(!array_key_exists($aRelation[$i], $aJoinedObjects)) {
                                    $aJoinedObjects[$aRelation[$i]] = $sRelations;
                                    $oQb->leftJoin($sPrevObject . '.' . $aRelation[$i], $sRelations);
                                    $sPrevObject = $sRelations;
                                    $sRelations++;
                                } else {
                                    $sPrevObject = $aJoinedObjects[$aRelation[$i]];
                                }
                                $i++;
                            }
                            $sWhere .= ($iKey2 !== 0 ? ', CONCAT(\' \', '. ($iKey2 != (sizeof($sColumn)-1) ? 'CONCAT(' : '') : '') . 'COALESCE (' . $sPrevObject . '.' . $aRelation[count($aRelation) - 1] .',\'\')';
                        } else {
                            $sWhere .= 'p.' . $sColumn2;
                        }
                    }
                    for($j=0;$j<count($sColumn)-1;$j++) {
                        $sWhere .= '))';
                    }
                    $sWhere .= ' LIKE :search';
                } else {
                    if (stristr($sColumn, '.') !== false) {
                        $aRelation = explode('.', $sColumn);
                        $i = 0;
                        $sPrevObject = 'p';
                        while ($i < count($aRelation) - 1) {
                            if(!array_key_exists($aRelation[$i], $aJoinedObjects)) {
                                $aJoinedObjects[$aRelation[$i]] = $sRelations;
                                $oQb->leftJoin($sPrevObject . '.' . $aRelation[$i], $sRelations);
                                $sPrevObject = $sRelations;
                                $sRelations++;
                            } else {
                                $sPrevObject = $aJoinedObjects[$aRelation[$i]];
                            }
                            $i++;
                        }
                        $sWhere .= $sPrevObject . '.' . $aRelation[count($aRelation) - 1] . ' LIKE :search OR ' . $sPrevObject . '.' . $aRelation[count($aRelation) - 1] . ' LIKE :search_literal';
                    } else {
                        $sWhere .= 'p.' . $sColumn . ' LIKE :search OR p.' . $sColumn . ' = :search_literal';
                    }
                }
            }
            $oQb->setParameter('search', '%' . $sSearch . '%');
            $oQb->setParameter('search_literal', $sSearch);
            $oQb->andWhere($sWhere);
        }

        foreach($aOrder as $sOrderField => $sOrderDir)
        {
            $sOrder = '';
            if (stristr($sOrderField, '.') !== false) {
                $aRelation = explode('.', $sOrderField);
                $i = 0;
                $sPrevObject = 'p';
                while ($i < count($aRelation) - 1) {
                    if(!array_key_exists($aRelation[$i], $aJoinedObjects)) {
                        $aJoinedObjects[$aRelation[$i]] = $sRelations;
                        $oQb->leftJoin($sPrevObject . '.' . $aRelation[$i], $sRelations);
                        $sPrevObject = $sRelations;
                        $sRelations++;
                    } else {
                        $sPrevObject = $aJoinedObjects[$aRelation[$i]];
                    }
                    $i++;
                }
                $sOrder .= $sPrevObject . '.' . $aRelation[count($aRelation) - 1];
            } else {
                $sOrder .= 'p.' . $sOrderField;
            }
            $oQb->addOrderBy($sOrder, $sOrderDir);
        }

        return $oQb->getQuery()->getSingleScalarResult();
    }

    public function getCount()
    {
        $oQb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('count(p)')
            ->from("DSJCMSDBBundle:Content\Slide", "p");

        return $oQb
            ->getQuery()
            ->getSingleScalarResult();
    }
}