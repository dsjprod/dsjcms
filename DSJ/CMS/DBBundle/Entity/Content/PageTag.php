<?php

namespace DSJ\CMS\DBBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * DSJ\CMS\DBBundle\Entity\System\PageTag
 *
 * @ORM\Table(name="link__page__tag")
 * @ORM\Entity()
 */
class PageTag
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Page $page
     *
     * @ORM\ManyToOne(targetEntity="DSJ\CMS\DBBundle\Entity\Content\Page", inversedBy="pageTags")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $page;

    /**
     * @var Tag $tag
     *
     * @ORM\ManyToOne(targetEntity="DSJ\CMS\DBBundle\Entity\System\Tag")
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $tag;

    public function __toString() {
        return $this->getTag()->__toString();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page
     *
     * @return PageTag
     */
    public function setPage($page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \DSJ\CMS\DBBundle\Entity\Content\Page 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set tag
     *
     * @param \DSJ\CMS\DBBundle\Entity\System\Tag $tag
     * @return PageTag
     */
    public function setTag(\DSJ\CMS\DBBundle\Entity\System\Tag $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \DSJ\CMS\DBBundle\Entity\System\Tag 
     */
    public function getTag()
    {
        return $this->tag;
    }
}
