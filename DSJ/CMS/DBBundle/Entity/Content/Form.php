<?php

namespace DSJ\CMS\DBBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * DSJ\CMS\DBBundle\Entity\Content\Form
 *
 * @ORM\Table(name="form")
 * @ORM\Entity()
 */
class Form
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * @var string $form
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $form;

    /**
     * @var string $actionRoute
     *
     * @ORM\Column(name="action_route", type="string", length=255, nullable=true)
     */
    protected $actionRoute;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Form
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set form
     *
     * @param string $form
     * @return Form
     */
    public function setForm($form)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return string 
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set actionRoute
     *
     * @param string $actionRoute
     * @return Form
     */
    public function setActionRoute($actionRoute)
    {
        $this->actionRoute = $actionRoute;

        return $this;
    }

    /**
     * Get actionRoute
     *
     * @return string 
     */
    public function getActionRoute()
    {
        return $this->actionRoute;
    }
}
