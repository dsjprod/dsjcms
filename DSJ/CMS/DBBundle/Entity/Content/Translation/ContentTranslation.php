<?php
/**
 * Created by PhpStorm.
 * User: george
 * Date: 1/23/14
 * Time: 5:40 PM
 */
namespace DSJ\CMS\DBBundle\Entity\Content\Translation;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractTranslation;

/**
 * @ORM\Table(name="translations__content", indexes={
 *      @ORM\Index(name="content_translation_idx", columns={"locale", "object_class", "field", "foreign_key"})
 * })
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 */
class ContentTranslation extends AbstractTranslation
{

}
