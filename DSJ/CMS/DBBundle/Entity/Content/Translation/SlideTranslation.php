<?php
namespace DSJ\CMS\DBBundle\Entity\Content\Translation;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractTranslation;

/**
 * @ORM\Table(name="translations__slide", indexes={
 *      @ORM\Index(name="slide_translation_idx", columns={"locale", "object_class", "field", "foreign_key"})
 * })
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 */
class SlideTranslation extends AbstractTranslation
{
}
