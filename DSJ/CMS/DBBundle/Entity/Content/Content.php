<?php

namespace DSJ\CMS\DBBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * DSJ\CMS\DBBundle\Entity\Content\Content
 *
 * @ORM\MappedSuperclass
 * @ORM\Table(name="content")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"backend" = "DSJ\CMS\DBBundle\Entity\Content\Content", "frontend" = "DSJ\CMS\FrontendDBBundle\Entity\Content\Content"})
 * @ORM\Entity(repositoryClass="DSJ\CMS\DBBundle\Entity\Content\Repository\ContentRepository")
 * @Gedmo\TranslationEntity(class="DSJ\CMS\DBBundle\Entity\Content\Translation\ContentTranslation")
 * @Vich\Uploadable
 */
class Content implements Translatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Post locale
     * Used locale to override Translation listener's locale
     *
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @var string $backendTitle
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="backend_title", type="string", length=255, nullable=true)
     */
    protected $backendTitle;

    /**
     * @var string $uniqueTitle
     *
     * @ORM\Column(name="unique_title", type="string", length=255, nullable=true)
     */
    protected $uniqueTitle;

    /**
     * @var string $slug
     *
     * @Gedmo\Slug(fields={"uniqueTitle"}, unique=true, updatable=true)
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @var string $title
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string $content
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="text", nullable=true)
     */
    protected $image;

    /**
     * @var File $imageFile
     *
     * @Vich\UploadableField(mapping="content_image", fileNameProperty="image")
     */
    protected $imageFile;

    /**
     * @var string $imageAlign
     *
     * @ORM\Column(name="image_align", type="string", length=255, nullable=true)
     */
    protected $imageAlign;

    /**
     * @var string $imageAltText
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="image_alt_text", type="string", length=255, nullable=true)
     */
    protected $imageAltText;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    protected $size;

    /**
     * @var PageContentType $type
     *
     * @ORM\ManyToOne(targetEntity="ContentType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="content_type_id", referencedColumnName="id", onDelete="cascade")
     * })
     */
    protected $type;

    /**
     * @var PageRow $pageRow
     *
     * @ORM\ManyToOne(targetEntity="PageRow", inversedBy="pageContent")
     * @ORM\JoinColumn(name="page_row_id", referencedColumnName="id", nullable=true)
     */
    protected $pageRow;

    /**
     * @var Form $form
     *
     * @ORM\ManyToOne(targetEntity="Form")
     * @ORM\JoinColumn(name="form_id", referencedColumnName="id", nullable=true)
     */
    protected $form;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer", nullable=true)
     */
    protected $sequence;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean", nullable=false )
     */
    protected $active = false;

    /**
     * @var string $link
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    protected $link;

    /**
     * @var string $linkText
     *
     * @ORM\Column(name="link_text", type="string", length=255, nullable=true)
     */
    protected $linkText;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Content
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Content
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Content
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Content
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
        if ($this->imageFile) {
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return string
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set imageAlign
     *
     * @param string $imageAlign
     * @return Content
     */
    public function setImageAlign($imageAlign)
    {
        $this->imageAlign = $imageAlign;

        return $this;
    }

    /**
     * Get imageAlign
     *
     * @return string 
     */
    public function getImageAlign()
    {
        return $this->imageAlign;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return Content
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Content
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Content
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Content
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Content
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set type
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\ContentType $type
     * @return Content
     */
    public function setType(\DSJ\CMS\DBBundle\Entity\Content\ContentType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \DSJ\CMS\DBBundle\Entity\Content\ContentType 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set pageRow
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\PageRow $pageRow
     * @return Content
     */
    public function setPageRow(\DSJ\CMS\DBBundle\Entity\Content\PageRow $pageRow = null)
    {
        $this->pageRow = $pageRow;

        return $this;
    }

    /**
     * Get pageRow
     *
     * @return \DSJ\CMS\DBBundle\Entity\Content\PageRow 
     */
    public function getPageRow()
    {
        return $this->pageRow;
    }

    /**
     * Sets translatable locale
     *
     * @param string $locale
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Set backendTitle
     *
     * @param string $backendTitle
     * @return Content
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;

        return $this;
    }

    /**
     * Get backendTitle
     *
     * @return string 
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Set form
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\Form $form
     * @return Content
     */
    public function setForm(\DSJ\CMS\DBBundle\Entity\Content\Form $form = null)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return \DSJ\CMS\DBBundle\Entity\Content\Form 
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set uniqueTitle
     *
     * @param string $uniqueTitle
     * @return Content
     */
    public function setUniqueTitle($uniqueTitle)
    {
        $this->uniqueTitle = $uniqueTitle;

        return $this;
    }

    /**
     * Get uniqueTitle
     *
     * @return string 
     */
    public function getUniqueTitle()
    {
        return $this->uniqueTitle;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Content
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set linkText
     *
     * @param string $linkText
     * @return Content
     */
    public function setLinkText($linkText)
    {
        $this->linkText = $linkText;

        return $this;
    }

    /**
     * Get linkText
     *
     * @return string 
     */
    public function getLinkText()
    {
        return $this->linkText;
    }

    /**
     * Set imageAltText
     *
     * @param string $imageAltText
     * @return Content
     */
    public function setImageAltText($imageAltText)
    {
        $this->imageAltText = $imageAltText;

        return $this;
    }

    /**
     * Get imageAltText
     *
     * @return string
     */
    public function getImageAltText()
    {
        return $this->imageAltText;
    }


}
