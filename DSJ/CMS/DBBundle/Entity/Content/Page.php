<?php

namespace DSJ\CMS\DBBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * DSJ\CMS\DBBundle\Entity\Content\Page
 *
 * @ORM\MappedSuperclass
 * @ORM\Table(name="page")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"backend" = "DSJ\CMS\DBBundle\Entity\Content\Page", "frontend" = "DSJ\CMS\FrontendDBBundle\Entity\Content\Page"})
 * @ORM\Entity(repositoryClass="DSJ\CMS\DBBundle\Entity\Content\Repository\PageRepository")
 * @Gedmo\TranslationEntity(class="DSJ\CMS\DBBundle\Entity\Content\Translation\PageTranslation")
 * @Vich\Uploadable
 */
class Page implements Translatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $page
     *
     * @ORM\Column(name="page", type="string", length=255, nullable=true, unique=true)
     */
    protected $page;

    /**
     * @var string $slug
     *
     * @Gedmo\Slug(fields={"pageTitle"}, unique=true, updatable=true)
     * @Gedmo\Translatable
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @var string $title
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string $menuTitle
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="menu_title", type="string", length=255, nullable=true)
     */
    protected $menuTitle;

    /**
     * @var string $pageTitle
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="page_title", type="string", length=255, nullable=true)
     */
    protected $pageTitle;

    /**
     * @var string $backendTitle
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="backend_title", type="string", length=255, nullable=true)
     */
    protected $backendTitle;

    /**
     * Post locale
     * Used locale to override Translation listener's locale
     *
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @var string $seoTitle
     *
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $seoTitle;

    /**
     * @var string $seoDescription
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="seodescription", type="text", nullable=true)
     */
    protected $seoDescription;

    /**
     * @var string $introduction
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="introduction", type="text", nullable=true)
     */
    protected $introduction;

    /**
     * @var string $content
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    protected $content;

    /**
     * @var string $imageFile
     *
     * @Vich\UploadableField(mapping="page_image", fileNameProperty="image")
     */
    protected $imageFile;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", nullable=true )
     */
    protected $image;

    /**
     * @var string $imageAlign
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $imageAlign;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean", nullable=false )
     */
    protected $active = false;

    /**
     * @var boolean $menuItem
     *
     * @ORM\Column(name="menu_item", type="boolean", nullable=false )
     */
    protected $menuItem = false;

    /**
     * @var boolean $breadcrumbItem
     *
     * @ORM\Column(name="breadcrumb_item", type="boolean", nullable=false )
     */
    protected $breadcrumbItem = true;

    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     **/
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\OneToMany(targetEntity="PageRow", mappedBy="page", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"sequence" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection $pageRows
     */
    protected $pageRows;

    /**
     * @ORM\OneToMany(targetEntity="DSJ\CMS\DBBundle\Entity\Content\PageTag", mappedBy="page", cascade={"persist", "remove"})
     */
    protected $pageTags;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer", nullable=true)
     */
    protected $sequence;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updated;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pageRows = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pageTags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set pageTitle
     *
     * @param string $pageTitle
     * @return Page
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;

        return $this;
    }

    /**
     * Get pageTitle
     *
     * @return string 
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * Set seoTitle
     *
     * @param string $seoTitle
     * @return Page
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    /**
     * Get seoTitle
     *
     * @return string 
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Set seoDescription
     *
     * @param string $seoDescription
     * @return Page
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    /**
     * Get seoDescription
     *
     * @return string 
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Set introduction
     *
     * @param string $introduction
     * @return Page
     */
    public function setIntroduction($introduction)
    {
        $this->introduction = $introduction;

        return $this;
    }

    /**
     * Get introduction
     *
     * @return string 
     */
    public function getIntroduction()
    {
        return $this->introduction;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Page
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
        if ($this->imageFile) {
            $this->updated = new \DateTime('now');
        }
    }

    /**
     * @return string
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Page
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set menuItem
     *
     * @param boolean $menuItem
     * @return Page
     */
    public function setMenuItem($menuItem)
    {
        $this->menuItem = $menuItem;

        return $this;
    }

    /**
     * Get menuItem
     *
     * @return boolean 
     */
    public function getMenuItem()
    {
        return $this->menuItem;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Page
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Page
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set parent
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\Page $parent
     * @return Page
     */
    public function setParent(\DSJ\CMS\DBBundle\Entity\Content\Page $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \DSJ\CMS\DBBundle\Entity\Content\Page 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\Page $children
     * @return Page
     */
    public function addChild(\DSJ\CMS\DBBundle\Entity\Content\Page $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\Page $children
     */
    public function removeChild(\DSJ\CMS\DBBundle\Entity\Content\Page $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function getMenuChildren($bActive=true)
    {
        $aChildren = $this->getChildren();

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("menuItem", true))
            ->orderBy(array("sequence" => Criteria::ASC));

        if($bActive === true) {
            $criteria
                ->andWhere(Criteria::expr()->eq("active", true));
        }

        $aMenuChildren = $aChildren->matching($criteria);

        return $aMenuChildren;
    }

    /**
     * Add pageRows
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\PageRow $pageRows
     * @return Page
     */
    public function addPageRow(\DSJ\CMS\DBBundle\Entity\Content\PageRow $pageRows)
    {
        $this->pageRows[] = $pageRows;

        return $this;
    }

    /**
     * Remove pageRows
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\PageRow $pageRows
     */
    public function removePageRow(\DSJ\CMS\DBBundle\Entity\Content\PageRow $pageRows)
    {
        $this->pageRows->removeElement($pageRows);
    }

    /**
     * Get pageRows
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPageRows()
    {
        return $this->pageRows;
    }

    /**
     * Sets translatable locale
     *
     * @param string $locale
     */
    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Set backendTitle
     *
     * @param string $backendTitle
     * @return Page
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;

        return $this;
    }

    /**
     * Get backendTitle
     *
     * @return string 
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Set imageAlign
     *
     * @param string $imageAlign
     * @return Page
     */
    public function setImageAlign($imageAlign)
    {
        $this->imageAlign = $imageAlign;

        return $this;
    }

    /**
     * Get imageAlign
     *
     * @return string 
     */
    public function getImageAlign()
    {
        return $this->imageAlign;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Page
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return Page
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set menuTitle
     *
     * @param string $menuTitle
     * @return Page
     */
    public function setMenuTitle($menuTitle)
    {
        $this->menuTitle = $menuTitle;

        return $this;
    }

    /**
     * Get menuTitle
     *
     * @return string 
     */
    public function getMenuTitle()
    {
        return $this->menuTitle;
    }

    /**
     * Add pageTags
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\PageTag $pageTags
     * @return Page
     */
    public function addPageTag(\DSJ\CMS\DBBundle\Entity\Content\PageTag $pageTags)
    {
        $pageTags->setPage($this);
        $this->pageTags[] = $pageTags;
        return $this;
    }

    /**
     * Remove pageTags
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\PageTag $pageTags
     */
    public function removePageTag(\DSJ\CMS\DBBundle\Entity\Content\PageRow $pageTags)
    {
        $this->pageTags->removeElement($pageTags);
    }

    /**
     * Get pageTags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPageTags()
    {
        return $this->pageTags;
    }

    /**
     * Set breadcrumbItem
     *
     * @param boolean $breadcrumbItem
     * @return Page
     */
    public function setBreadcrumbItem($breadcrumbItem)
    {
        $this->breadcrumbItem = $breadcrumbItem;

        return $this;
    }

    /**
     * Get breadcrumbItem
     *
     * @return boolean
     */
    public function getBreadcrumbItem()
    {
        return $this->breadcrumbItem;
    }

}
