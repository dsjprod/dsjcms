<?php
namespace DSJ\CMS\DBBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ContentType
 *
 * @ORM\Table(name="content_type")
 * @ORM\Entity()
 */
class ContentType {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string $twig
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $twig;

    /**
     * @var string $fields
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $fields;

    /**
     * @var string $content
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    function __toString()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ContentType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set twig
     *
     * @param string $twig
     * @return ContentType
     */
    public function setTwig($twig)
    {
        $this->twig = $twig;

        return $this;
    }

    /**
     * Get twig
     *
     * @return string 
     */
    public function getTwig()
    {
        return $this->twig;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ContentType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set fields
     *
     * @param string $fields
     * @return ContentType
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * Get fields
     *
     * @return string 
     */
    public function getFields()
    {
        return $this->fields;
    }
}
