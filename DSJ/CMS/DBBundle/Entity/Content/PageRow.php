<?php
/**
 * Created by PhpStorm.
 * User: george
 * Date: 1/30/14
 * Time: 6:24 PM
 */

namespace DSJ\CMS\DBBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * DSJ\CMS\DBBundle\Entity\Content\PageRow
 *
 * @ORM\Table(name="page_row")
 * @ORM\Entity()
 */
class PageRow {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Page $page
     *
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="pageRows")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", nullable=true)
     */
    protected $page;

    /**
     * @ORM\OneToMany(targetEntity="Content", mappedBy="pageRow", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"sequence" = "ASC"})
     * @var \Doctrine\Common\Collections\Collection $pageContent
     */
    protected $pageContent;

    /**
     * @ORM\ManyToOne(targetEntity="DSJ\CMS\DBBundle\Entity\System\Color")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $bgColor;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer", nullable=true)
     */
    protected $sequence;

    /**
     * @var boolean $fullWidth
     *
     * @ORM\Column(name="full_width", type="boolean", nullable=false )
     */
    protected $fullWidth = false;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updated;

    public function __construct()
    {
        $this->pageContent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return PageRow
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PageRow
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return PageRow
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set page
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\Page $page
     * @return PageRow
     */
    public function setPage(\DSJ\CMS\DBBundle\Entity\Content\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \DSJ\CMS\DBBundle\Entity\Content\Page 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Add pageContent
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\Content $pageContent
     * @return PageRow
     */
    public function addPageContent(\DSJ\CMS\DBBundle\Entity\Content\Content $pageContent)
    {
        $this->pageContent[] = $pageContent;

        return $this;
    }

    /**
     * Remove pageContent
     *
     * @param \DSJ\CMS\DBBundle\Entity\Content\Content $pageContent
     */
    public function removePageContent(\DSJ\CMS\DBBundle\Entity\Content\Content $pageContent)
    {
        $this->pageContent->removeElement($pageContent);
    }

    /**
     * Get pageContent
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPageContent()
    {
        return $this->pageContent;
    }

    /**
     * Set fullWidth
     *
     * @param boolean $fullWidth
     * @return PageRow
     */
    public function setFullWidth($fullWidth)
    {
        $this->fullWidth = $fullWidth;

        return $this;
    }

    /**
     * Get fullWidth
     *
     * @return boolean 
     */
    public function getFullWidth()
    {
        return $this->fullWidth;
    }

    /**
     * Set bgColor
     *
     * @param \DSJ\CMS\DBBundle\Entity\System\Color $bgColor
     * @return PageRow
     */
    public function setBgColor(\DSJ\CMS\DBBundle\Entity\System\Color $bgColor = null)
    {
        $this->bgColor = $bgColor;

        return $this;
    }

    /**
     * Get bgColor
     *
     * @return \DSJ\CMS\DBBundle\Entity\System\Color 
     */
    public function getBgColor()
    {
        return $this->bgColor;
    }
}
