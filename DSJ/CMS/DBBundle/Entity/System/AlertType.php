<?php

namespace DSJ\CMS\DBBundle\Entity\System;


/**
 * DSJ\CMS\DBBundle\Entity\System\AlertType
 *
 */
class AlertType {

    protected $type = 'AlertType';

    const TYPE_SUCCESS = 1;
    const TYPE_INFO = 2;
    const TYPE_ERROR = 3;

    public static function getChoices() {
        $array = array(
            self::TYPE_SUCCESS  => 'success',
            self::TYPE_INFO     => 'info',
            self::TYPE_ERROR    => 'error',
        );
        return $array;
    }
    
    public static function getSelectChoices() {
    	$array = array(
    			self::TYPE_SUCCESS 		=> 'Bevestiging/succesvol',
    			self::TYPE_INFO         => 'Informatie',
    			self::TYPE_ERROR        => 'Foutmelding',
    	);
    	return $array;
    }
    
    public function getType($iId) {
    	$aTypes = $this->getChoices();
    	
    	return $aTypes[$iId];
    }
    
    public function getSelectType($iId){
    	$aTypes = $this->getSelectChoices();
    	 
    	return $aTypes[$iId];
    }
}