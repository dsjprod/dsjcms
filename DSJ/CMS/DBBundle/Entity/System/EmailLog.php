<?php

namespace DSJ\CMS\DBBundle\Entity\System;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * DSJ\CMS\DBBundle\Entity\System\EmailLog
 *
 * @ORM\Table(name="email_log")
 * @ORM\Entity()
 */
class EmailLog
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $to
     *
     * @ORM\Column(name="to_email", type="string", length=255, nullable=true)
     */
    private $to;

    /**
     * @var string $from
     *
     * @ORM\Column(name="from_email", type="string", length=255, nullable=true)
     */
    private $from;

    /**
     * @var string $subject
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var text $content
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string $attachments
     *
     * @ORM\Column(name="attachments", type="text", nullable=true)
     */
    private $attachments;

    /**
     * @var datetime $sent
     *
     * @ORM\Column(name="sent", type="datetime", nullable=true)
     */
    private $sent;

    /**
     * @var Email $email
     *
     * @ORM\ManyToOne(targetEntity="Email")
     * @ORM\JoinColumn(name="email_id", referencedColumnName="id", nullable=true)
     */
    protected $email;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set to
     *
     * @param string $to
     * @return EmailLog
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return string 
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set from
     *
     * @param string $from
     * @return EmailLog
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return string 
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return EmailLog
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return EmailLog
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set attachments
     *
     * @param string $attachments
     * @return EmailLog
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return string 
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set sent
     *
     * @param \DateTime $sent
     * @return EmailLog
     */
    public function setSent($sent)
    {
        $this->sent = $sent;

        return $this;
    }

    /**
     * Get sent
     *
     * @return \DateTime 
     */
    public function getSent()
    {
        return $this->sent;
    }

    /**
     * Set email
     *
     * @param \DSJ\CMS\DBBundle\Entity\System\Email $email
     * @return EmailLog
     */
    public function setEmail(\DSJ\CMS\DBBundle\Entity\System\Email $email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return \DSJ\CMS\DBBundle\Entity\System\Email 
     */
    public function getEmail()
    {
        return $this->email;
    }
}
