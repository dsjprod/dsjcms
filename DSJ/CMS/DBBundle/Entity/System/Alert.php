<?php
namespace DSJ\CMS\DBBundle\Entity\System;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * DSJ\CMS\DBBundle\Entity\System\Alert
 *
 * @ORM\Table(name="alert")
 * @Gedmo\TranslationEntity(class="DSJ\CMS\DBBundle\Entity\System\Translation\AlertTranslation")
 * @ORM\Entity(repositoryClass="DSJ\CMS\DBBundle\Entity\System\Repository\AlertRepository")
 */
class Alert
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $identifier
     *
     * @ORM\Column(name="identifier", type="string", length=55, nullable=false)
     */
    protected $identifier;

    /**
     * @var string $alert
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="alert", type="string", length=255, nullable=false)
     */
    protected $alert;

    /**
     * @var AlertType $type
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    protected $type;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return Alert
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string 
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set alert
     *
     * @param string $alert
     * @return Alert
     */
    public function setAlert($alert)
    {
        $this->alert = $alert;

        return $this;
    }

    /**
     * Get alert
     *
     * @return string 
     */
    public function getAlert()
    {
        return $this->alert;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Alert
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Alert
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }
}
