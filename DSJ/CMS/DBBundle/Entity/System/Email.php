<?php

namespace DSJ\CMS\DBBundle\Entity\System;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * DSJ\CMS\DBBundle\Entity\System\Email
 *
 * @ORM\Table(name="email")
 * @ORM\Entity(repositoryClass="DSJ\CMS\DBBundle\Entity\System\Repository\EmailRepository")
 * @Gedmo\TranslationEntity(class="DSJ\CMS\DBBundle\Entity\System\Translation\EmailTranslation")
 */
class Email implements Translatable
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string $contentTitle
     *
     * @ORM\Column(name="content_title", type="string", length=255, nullable=true)
     */
    private $contentTitle;

    /**
     * @var string $subject
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var text $content
     *
     * @Gedmo\Translatable
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string $identifier
     *
     * @ORM\Column(name="identifier", type="string", length=255, nullable=true)
     */
    protected $identifier;

    /**
     * @var string $slug
     *
     * @Gedmo\Slug(fields={"identifier"}, unique=true, updatable=true)
     * @ORM\Column(name="slug", type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @Gedmo\Locale
     * Used locale to override Translation listener`s locale
     * this is not a mapped field of entity metadata, just a simple property
     */
    private $locale;

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Email
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return Email
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Email
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return Email
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Email
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set contentTitle
     *
     * @param string $contentTitle
     * @return Email
     */
    public function setContentTitle($contentTitle)
    {
        $this->contentTitle = $contentTitle;

        return $this;
    }

    /**
     * Get contentTitle
     *
     * @return string
     */
    public function getContentTitle()
    {
        return $this->contentTitle;
    }
}
