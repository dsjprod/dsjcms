<?php
namespace DSJ\CMS\DBBundle\Entity\System;

use Doctrine\ORM\Mapping as ORM;

/**
 * DSJ\CMS\DBBundle\Entity\System\Color
 *
 * @ORM\Table(name="color")
 * @ORM\Entity()
 */
class Color
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * @var string $hexCode
     *
     * @ORM\Column(name="hex_code", type="string", length=6, nullable=true)
     */
    protected $hexCode;

    /**
     * @var string $textHexCode
     *
     * @ORM\Column(name="text_hex_code", type="string", length=6, nullable=true)
     */
    protected $textHexCode;

    /**
     * @var string $className
     *
     * @ORM\Column(name="class_name", type="string", length=255, nullable=true)
     */
    protected $className;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="active", type="boolean", nullable=false )
     */
    protected $active = false;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Color
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set hexCode
     *
     * @param string $hexCode
     * @return Color
     */
    public function setHexCode($hexCode)
    {
        $this->hexCode = $hexCode;

        return $this;
    }

    /**
     * Get hexCode
     *
     * @return string 
     */
    public function getHexCode()
    {
        return $this->hexCode;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Color
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    public function getTitleAndColorCodes()
    {
        return $this->getTitle()."__#".$this->getHexCode()."__#".$this->getTextHexCode();
    }

    /**
     * Set textHexCode
     *
     * @param string $textHexCode
     * @return Color
     */
    public function setTextHexCode($textHexCode)
    {
        $this->textHexCode = $textHexCode;

        return $this;
    }

    /**
     * Get textHexCode
     *
     * @return string 
     */
    public function getTextHexCode()
    {
        return $this->textHexCode;
    }

    /**
     * Set className
     *
     * @param string $className
     * @return Color
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }

    /**
     * Get className
     *
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }
}
