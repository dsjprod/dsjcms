<?php
namespace DSJ\CMS\DBBundle\Entity\System\Translation;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractTranslation;

/**
 * @ORM\Table(name="translations__alert", indexes={
 *      @ORM\Index(name="alert_translation_idx", columns={"locale", "object_class", "field", "foreign_key"})
 * })
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 */
class AlertTranslation extends AbstractTranslation
{

}
