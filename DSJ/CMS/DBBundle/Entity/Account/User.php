<?php

namespace DSJ\CMS\DBBundle\Entity\Account;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * DSJ\CMS\DBBundle\Entity\Account\User
 *
 * @ORM\Table(name="admin_user")
 * @ORM\Entity()
 * @Vich\Uploadable
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    protected $email;

    /**
     * @var string $salt
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=false)
     */
    protected $salt;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    protected $password;

    /**
     * @var string $imageFile
     *
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="image")
     */
    protected $imageFile;

    /**
     * @var string $image
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image;
    
    /**
     * @var datetime $lastLogin
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    protected $lastLogin;
    
    /**
     * @var datetime $currentLogin
     *
     * @ORM\Column(name="current_login", type="datetime", nullable=true)
     */
    protected $currentLogin;

    /**
     * @var datetime $createdAt
     *
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $created;

    /**
     * @var datetime $updatedAt
     *
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updated;

    public function __sleep() {
        return array('id', 'name', 'email', 'password', 'salt');
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return Role[] The user roles
     */
    function getRoles()
    {
       return array("ROLE_ADMIN");
    }

   /**
    * Returns the username used to authenticate the user.
    *
    * @return string The username
    */
    function getUsername()
    {
       return $this->getEmail();
    }

   /**
    * Removes sensitive data from the user.
    *
    * @return void
    */
    function eraseCredentials()
    {
    }

   /**
    * The equality comparison should neither be done by referential equality
    * nor by comparing identities (i.e. getId() === getId()).
    *
    * However, you do not need to compare every attribute, but only those that
    * are relevant for assessing whether re-authentication is required.
    *
    * @param UserInterface $user
    * @return Boolean
    */
   function equals(UserInterface $user)
   {
       return ($user->getUsername() == $this->getUsername());
   }

   public function getSalt() {
       return $this->salt;
   }

   public function getPassword() {
       return $this->password;
   }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Set createdAt
     *
     * @param datetime $created
     */
    public function setCreated($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updatedAt
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param string $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
        
        if ($this->imageFile) {
        	$this->updated = new \DateTime('now');
        }
    }

    /**
     * @return string
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     * @return User
     */
    public function setLastLogin($lastLogin)
    {
        $this->lastLogin = $lastLogin;
    
        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime 
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set currentLogin
     *
     * @param \DateTime $currentLogin
     * @return User
     */
    public function setCurrentLogin($currentLogin)
    {
        $this->currentLogin = $currentLogin;
    
        return $this;
    }

    /**
     * Get currentLogin
     *
     * @return \DateTime 
     */
    public function getCurrentLogin()
    {
        return $this->currentLogin;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return User
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
}
